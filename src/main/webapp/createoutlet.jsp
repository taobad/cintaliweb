
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.google.appengine.api.blobstore.BlobstoreService, com.google.appengine.api.blobstore.BlobstoreServiceFactory, java.util.List, java.util.ArrayList, com.cinttali.model.*, com.cinttali.manager.*"%>

<%
    BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
    List<Location> locs = (List<Location>)request.getAttribute("locs");
    List<OutletCategory> cats = (List<OutletCategory>)request.getAttribute("cats");
    //so we have an empty categories combo-box
    cats = new ArrayList<OutletCategory>();
%>
<head>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">

<div class="row">
<div class="col-xs-12 col-md-8 col-md-offset-2">

      <form action="<%= blobstoreService.createUploadUrl("/waiter") %>" method="post" enctype="multipart/form-data">
      
        <h3 class="form-signin-heading" id="page-title" >Create Menu Item</h3>

        <div class="form-group">
            <label for="location">Location</label>
            <select id="location" name="lc" class="form-control" placeholder="Location" onchange="getOutletCats()" required />
                <option value="na">Select Location</option>
                <% //we select the saved location
                 for(Location lc : locs){ %>
                    <option value='<%=lc.getLocationCode()%>'><%=lc.getName()%></option>
                
                <% } %>
                
            </select>
        </div>
      
       <div class="form-group">
            <label for="outletcode">Outlet Code</label>
            <input type="text" id="outletcode" name="oc" class="form-control" placeholder="outlet code"  />
        </div>
      
      <div class="form-group">
            <label for="outlet">Outlet</label>
            <input type="text" id="outlet" name="on" class="form-control" placeholder="outlet name"  />
        </div>
        
        <div class="form-group">
            <label for="desc">Description</label>
            <textarea cols="10" rows="5" id="desc" name="desc" class="form-control"></textarea>
        </div>
      
        <div class="form-group">
            <label for="dc">Delivery Charge</label>
            <input type="text" id="dc" name="dc" class="form-control" placeholder="delivery charge" required autofocus />
        </div>
      
      <div class="form-group">
            <label for="location">Category</label>
            <select id="category" name="ocat" class="form-control" placeholder="Category" required />
                <% 
                 for(OutletCategory oc : cats){ %>
                    <option value='<%=oc.getCatCode()%>'><%=oc.getName()%></option>
                
                <% } %>
            </select>
        </div>
     
        <input type="file" name="myFile">
        <div class="form-group">
            <button class="btn btn-lg btn-primary" id="btnsub" type="submit">Save</button>
        </div>
     
      <input type="hidden" name="action" value="coutlet" />
      </form>
      
</div> <!-- /col -->
</div> <!-- /row -->

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster-->
    <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> 
    <script type="text/javascript" src="/js/l.js" ></script>
    
	      
  </body>
</html>
