function getOutlets(){
	var loc = $("#location option:selected").val();
	$.ajax({
 		  type: "GET",
 		  url: '/waiter?action=outlets&loc='+ loc, 		  
 		  dataType: 'json',
 		  success: function(data){
 			  
 			populate($("#outlet"), data);
 		  },
 		  error: function(data){
 			
 		  }
 		});
}

function getOutletCats(){
	var loc = $("#location option:selected").val();
	$.ajax({
 		  type: "GET",
 		  url: '/waiter?action=loccats&loc='+ loc, 		  
 		  dataType: 'json',
 		  success: function(data){
 			  
 			populateOutletCats($("#category"), data);
 		  },
 		  error: function(data){
 			
 		  }
 		});
}

function populate(selector, data){
	for(var i = 0; i < data.payload.length; i++){
		$(selector)
		.append('<option value='+data.payload[i].outletCode + '>' + data.payload[i].name + '</option>');
		  
	  }
	
}

function populateOutletCats(selector, data){	
	for(var i = 0; i < data.payload.length; i++){
		$(selector)
		.append('<option value='+data.payload[i].catCode + '>' + data.payload[i].name + '</option>');
		  
	  }
	
}