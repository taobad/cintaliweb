
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.google.appengine.api.blobstore.BlobstoreService, com.google.appengine.api.blobstore.BlobstoreServiceFactory, java.util.List, com.cinttali.model.*, com.cinttali.manager.*"%>

<%
    BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
    MenuItem mi = (MenuItem)request.getAttribute("mitem");
    List<MenuCategory> menuCats = (List<MenuCategory>)request.getAttribute("mcats");
    List<Location> locs = (List<Location>)request.getAttribute("locs");
    
    
    boolean edit = false;
    String itemCode = null;		
	String name = null;	
	int price = 0;
	String category = null;
	String pckDesc = null;
	String nutrition = null;
	String location = null;
	boolean availability = false;
	Outlet outlet = null;
	
    if(mi != null){
        outlet = new OutletManager().getOutlet(mi.getOutlet());
        edit = true;
        itemCode = mi.getItemCode();
        name = mi.getName();
        price = mi.getPrice();
        category = mi.getCategory();
        pckDesc = mi.getPckDesc();
        nutrition = mi.getNutrition();
        location = mi.getLocation();
        availability = mi.isAvailable();
        System.out.println("--- " + name + " " + price);
    }
%>
<head>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">

<div class="row">
<div class="col-xs-12 col-md-8 col-md-offset-2">

      <form action="<%= blobstoreService.createUploadUrl("/waiter") %>" method="post" enctype="multipart/form-data">
      
        <h3 class="form-signin-heading" id="page-title" >Create Menu Item</h3>

        <div class="form-group">
            <label for="location">Location</label>
            <select id="location" name="location" class="form-control" placeholder="Location" required <% if(!edit){ %>onchange="getOutlets()" <% } %> >
               <option value="na">Select Location</option>
                <% //we select the saved location
                if(edit){ for(Location lc : locs){ %>
                    <option value='<%=lc.getLocationCode()%>' <%if(location.equals(lc.getLocationCode())){%> selected <%}%> ><%=lc.getName()%></option>
                <% } } else{ for(Location lc : locs){ %>
                    <option value='<%=lc.getLocationCode()%>'><%=lc.getName()%></option>
                
                <% } } %>
            </select>
        </div>
      
      <div class="form-group">
            <label for="outlet">Outlet</label>
            
            <select id="outlet" name="outlet" class="form-control" placeholder="Outlet"  >
            <% if(edit){ %>
                <option value='<%=outlet.getOutletCode()%>' > <%= outlet.getName() %> </option>
                <% } %>
                
            </select>
        </div>
      
        <div class="form-group">
            <label for="itemcode">Item Code</label>
            <input type="text" id="itemcode" name="itemcode" class="form-control" placeholder="item code" required autofocus <%if(edit){%> disabled <%}%> <%if(itemCode != null){%>value='<%=itemCode%>'<%}%> >
        </div>
      
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" id="name" name="name" class="form-control" placeholder="Name" required <%if(name != null){%> value='<%=name%>' <%}%> >
        </div>
      
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" id="price" name="price" class="form-control" placeholder="Price" required <%if(price != 0){%>value='<%=price%>'<%}%> >
        </div>
        <div class="form-group">
            <label for="category">Category</label>
            <select id="category" name="category" class="form-control" placeholder="Category" required >
                <% //we select the saved category
                if(edit){  for(MenuCategory mc : menuCats){ %>
                    <option value='<%=mc.getCatCode()%>' <%if(category.equals(mc.getCatCode())){%> selected <%}%> ><%=mc.getName()%></option>
                <% } } else { for(MenuCategory mc : menuCats){ %>
                     <option value='<%=mc.getCatCode()%>'><%=mc.getName()%></option>
                <% } } %>
            </select>
        </div>
        
        <div class="form-group">
            <label for="desc">Package Description</label>
            <textarea id="desc" name="desc" class="form-control" rows="3" cols="10"><%if(pckDesc != null){%><%=pckDesc%><%}%></textarea>
        </div>
        <div class="form-group">
            <label for="nutrition">Nutrition/Health Info</label>
            <textarea id="nutrition" name="nutrition" class="form-control" rows="3" cols="10"><%if(nutrition != null){%><%=nutrition%><%}%></textarea>
        </div>
        
        <div class="form-group">
            <label for="avail">Availability</label>
            <select id="avail" name="avail" class="form-control" placeholder="availability" required >
                <% //we select the saved category
                if(edit){  if(availability) {%>
                    <option value='true' selected >Yes</option>
                    <option value='false' >No</option>
                    <% } else { %> 
                    <option value='true' >Yes</option>
                    <option value='false' selected >No</option>
                    <% } %>
                <% }  else {  %>
                     <option value='true' >Yes</option>
                     <option value='false' >No</option>
                <% }  %>
            </select>
        </div>
        <% if(edit){ %>
        <div class="form-group">            
            <input type="checkbox" id="remove" name="remove" /> Delete
        </div>
        <% } %>
        <input type="file" name="myFile">
        <div class="form-group">
            <button class="btn btn-lg btn-primary" id="btnsub" type="submit">Save</button>
        </div>
        
        <% if(edit){ %>
            <input type="hidden" name="edit" value="true" />
            <input type="hidden" name="itemcodeedit" value="<%=itemCode%>" />
        <%}%>
      <input type="hidden" name="action" value="si" />
      </form>
      
</div> <!-- /col -->
</div> <!-- /row -->

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster-->
    <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> 
    <script type="text/javascript" src="/js/l.js" ></script>
    
	      
  </body>
</html>
