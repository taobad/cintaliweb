
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List, com.cinttali.model.MenuItem"%>
<%
    List<MenuItem> menuItems = (List<MenuItem>)request.getAttribute("menuitems");
%>
<head>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">

<div class="row">
<div class="col-xs-12 col-md-8 col-md-offset-2">

      <form action="/waiter" method="post" >
      
        <h3 class="form-signin-heading" id="page-title" >Select Menu Item to edit</h3>
      
        
        <div class="form-group">
            <label for="category">Menu Items</label>
            <select id="mi" name="mi" class="form-control" placeholder="Menu Items" required>
                <% for(MenuItem mi : menuItems){ %>
                    <option value='<%=mi.getItemCode()%>'><%=mi.getName()%></option>
                <% } %>
            </select>
        </div>
        
        
        <div class="form-group">
            <button class="btn btn-lg btn-primary" id="btnsub" type="submit">Save</button>
        </div>
      <input type="hidden" name="action" value="editmi" />
      </form>
      
</div> <!-- /col -->
</div> <!-- /row -->

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster-->
    <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> 
    <script type="text/javascript" src="/js/local-min.js" ></script>
    
	      
  </body>
</html>
