package com.cinttali.utility;

import java.util.List;

import com.cinttali.model.DeliverySchedule;
import com.cinttali.model.MenuCategory;
import com.cinttali.model.MenuItem;
import com.cinttali.model.Outlet;
import com.cinttali.model.OutletCategory;
import com.cinttali.model.Street;

public class LocationRelatedObjects {

	private int version;
	private List<OutletCategory> outletCategories;
	private List<Outlet> outlets;
	private List<Street> streets;
	private List<DeliverySchedule> delvSchs;
	private List<MenuItem> menuItems;
	private List<MenuCategory> menuCats;
	
	public LocationRelatedObjects(){
		
	}
	
	
	public LocationRelatedObjects(List<OutletCategory> oCats, List<Outlet> outlets, List<Street> streets,
			List<DeliverySchedule> delvSchs, List<MenuItem> menuItems,
			List<MenuCategory> menuCats) {
		super();
		this.outletCategories = oCats;
		this.outlets = outlets;
		this.streets = streets;
		this.delvSchs = delvSchs;
		this.menuItems = menuItems;
		this.menuCats = menuCats;
	}
	
	
	public int getVersion() {
		return version;
	}


	public void setVersion(int version) {
		this.version = version;
	}


	public List<OutletCategory> getOutletCategories() {
		return outletCategories;
	}


	public void setOutletCategories(List<OutletCategory> outletCategories) {
		this.outletCategories = outletCategories;
	}


	public List<Outlet> getOutlets() {
		return outlets;
	}
	public void setOutlets(List<Outlet> outlets) {
		this.outlets = outlets;
	}
	public List<Street> getStreets() {
		return streets;
	}
	public void setStreets(List<Street> streets) {
		this.streets = streets;
	}
	public List<DeliverySchedule> getDelvSchs() {
		return delvSchs;
	}
	public void setDelvSchs(List<DeliverySchedule> delvSchs) {
		this.delvSchs = delvSchs;
	}
	public List<MenuItem> getMenuItems() {
		return menuItems;
	}
	public void setMenuItems(List<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}
	public List<MenuCategory> getMenuCats() {
		return menuCats;
	}
	public void setMenuCats(List<MenuCategory> menuCats) {
		this.menuCats = menuCats;
	}
	
	
	
}
