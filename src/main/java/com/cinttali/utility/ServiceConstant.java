package com.cinttali.utility;

public enum ServiceConstant {

	CASH_ON_DELIVERY("cod"),
	PAY_ONLINE_GATEWAY("pog"),
	SERVICE_CHARGE("sc");
	
	private String value;
    private ServiceConstant(String value){
        this.value = value;
    }
    
    public String getValue(){
    	return this.value;
    }
    
}
