package com.cinttali.utility;

import java.util.HashMap;

import com.cinttali.model.User;

/**
 * Created by buls on 6/12/15.
 */
public class OrderPackage {

    User user;
    HashMap<String, Integer> OrderItems; //key: MItemId, value: quantity
    String outletCode;
    String deliveryTime;

    public OrderPackage() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public HashMap<String, Integer> getOrderItems() {
        return OrderItems;
    }

    public void setOrderItems(HashMap<String, Integer> orderItems) {
        OrderItems = orderItems;
    }

    public void setOrderItem(String mItem, Integer quantity) {
        OrderItems.put(mItem, quantity);
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

	public String getOutletCode() {
		return outletCode;
	}

	public void setOutletCode(String outletCode) {
		this.outletCode = outletCode;
	}
    
    
}
