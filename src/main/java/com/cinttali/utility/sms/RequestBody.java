package com.cinttali.utility.sms;

/*
 * -------------------------------------------------------------------------------------------
 * Updated: Sep 4, 2014  10:49:42 AM
 * This source code can only be used and altered together with SMSPimp system.
 *
 * Revision: 1.0	
 * 
 * Requirements:
 * You need to have an SMSPimp sub account.
 * Register at: https://smspimp.com
 * 
 * Support: developer@smspimp.com
 * -------------------------------------------------------------------------------------------
 */


public abstract class RequestBody {

    private String username; //sub account name
    private String password; //sub account password
    private String userId;// your email address on smspimp.com

    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
