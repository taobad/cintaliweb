package com.cinttali.utility.sms;
/*
 * -------------------------------------------------------------------------------------------
 * Updated: Sep 4, 2014  10:49:42 AM
 * This source code can only be used and altered together with SMSPimp system.
 *
 * Revision: 1.0	
 * 
 * Requirements:
 * You need to have an SMSPimp sub account.
 * Register at: https://smspimp.com
 * 
 * Support: developer@smspimp.com
 * -------------------------------------------------------------------------------------------
 */

public class HttpDeliveryBody extends RequestBody {

    private String ids; //comma seperated ids of  the message you want their delivery status
    private String url = "https://smspimp.com/api/getreports"; // the url of smspimp  getting report

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
