package com.cinttali.utility.sms;

/*
 * -------------------------------------------------------------------------------------------
 * Updated: Sep 4, 2014  10:49:42 AM
 * This source code can only be used and altered together with SMSPimp system.
 *
 * Revision: 1.0	
 * 
 * Requirements:
 * You need to have an SMSPimp sub account.
 * Register at: https://smspimp.com
 * 
 * Support: developer@smspimp.com
 * -------------------------------------------------------------------------------------------
 */
public class HttpRequestBody extends RequestBody {

    private String requestType = "json"; //xml or json default is json
    private String url = "http://smsbuzz.ng/api/send"; // the url of smspimp  sending api
    private String destAddr; //Destination address (recipient), comma separated phone numbers
    private String sourceAddr; //Source address (sender). maximum of 11 characters
    private String message; //your message

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDestAddr() {
        return destAddr;
    }

    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    public String getSourceAddr() {
        return sourceAddr;
    }

    public void setSourceAddr(String sourceAddr) {
        this.sourceAddr = sourceAddr;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
