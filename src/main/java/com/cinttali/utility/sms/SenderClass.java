package com.cinttali.utility.sms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/*
 * -------------------------------------------------------------------------------------------
 * Updated: Sep 4, 2014  10:49:42 AM
 * This source code can only be used and altered together with SMSPimp system.
 *
 * Revision: 1.0	
 * 
 * Requirements:
 * You need to have an SMSPimp sub account.
 * Register at: https://smspimp.com
 * 
 * Support: developer@smspimp.com
 * -------------------------------------------------------------------------------------------
 */

public class SenderClass {

    /**
     * Represents an HTTP connection
     */
    private static HttpURLConnection httpConn;
    
    /**
     * 
     * @param httpDeliveryBody
     * @return response from smspimp server
     */
    public String getDeliveryReports(HttpDeliveryBody httpDeliveryBody) throws UnsupportedEncodingException{
        // test sending POST request
        Map<String, String> params = new HashMap<>();
        params.put("USERNAME", URLEncoder.encode(httpDeliveryBody.getUsername(),  "UTF-8"));
        params.put("USERID", URLEncoder.encode(httpDeliveryBody.getUserId(), "UTF-8"));
        params.put("PASSWORD",URLEncoder.encode( httpDeliveryBody.getPassword(), "UTF-8"));
        params.put("IDS",URLEncoder.encode( httpDeliveryBody.getIds(), "UTF-8"));
        String response = "";
        try {
            SenderClass.sendPostRequest(httpDeliveryBody.getUrl(), params);
            response = SenderClass.readSingleLineRespone();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        finally{
            SenderClass.disconnect();
        }
        return response;
    }

    /**
     * 
     * @param httpRequestBody
     * @return  response from smspimp server
     */
    public String send(HttpRequestBody httpRequestBody) throws UnsupportedEncodingException {
        // test sending POST request
        Map<String, String> params = new HashMap<>();
        params.put("USERNAME", URLEncoder.encode(httpRequestBody.getUsername(), "UTF-8"));
        params.put("USERID", URLEncoder.encode(httpRequestBody.getUserId(), "UTF-8"));
        params.put("PASSWORD", URLEncoder.encode(httpRequestBody.getPassword(), "UTF-8"));
        params.put("MESSAGE", URLEncoder.encode(httpRequestBody.getMessage(), "UTF-8"));
        params.put("SOURCEADDR", URLEncoder.encode(httpRequestBody.getSourceAddr(), "UTF-8"));
        params.put("DESTADDR", URLEncoder.encode(httpRequestBody.getDestAddr(), "UTF-8"));
        String response = "";
        try {
            SenderClass.sendPostRequest(httpRequestBody.getUrl(), params);
            response = SenderClass.readSingleLineRespone();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        finally{
            SenderClass.disconnect();
        }
        return response;
    }
    
    
     /**
     * Makes an HTTP request using POST method to the specified URL.
     * 
     * @param requestURL
     *            the URL of the remote server
     * @param params
     *            A map containing POST data in form of key-value pairs
     * @return An HttpURLConnection object
     * @throws IOException
     *             thrown if any I/O error occurred
     */
    public static HttpURLConnection sendPostRequest(String requestURL,
            Map<String, String> params) throws IOException {
        URL url = new URL(requestURL);
        httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setUseCaches(false);
 
        httpConn.setDoInput(true); // true indicates the server returns response
 
        StringBuilder requestParams = new StringBuilder();
 
        if (params != null && params.size() > 0) {
 
            httpConn.setDoOutput(true); // true indicates POST request
 
            // creates the params string, encode them using URLEncoder
            Iterator<String> paramIterator = params.keySet().iterator();
            while (paramIterator.hasNext()) {
                String key = paramIterator.next();
                String value = params.get(key);
                requestParams.append(URLEncoder.encode(key, "UTF-8"));
                requestParams.append("=").append(
                        URLEncoder.encode(value, "UTF-8"));
                requestParams.append("&");
            }
 
            // sends POST data
            OutputStreamWriter writer = new OutputStreamWriter(
                    httpConn.getOutputStream());
            writer.write(requestParams.toString());
            writer.flush();
        }
 
        return httpConn;
    }
 
    /**
     * Returns only one line from the server's response. This method should be
     * used if the server returns only a single line of String.
     * 
     * @return a String of the server's response
     * @throws IOException
     *             thrown if any I/O error occurred
     */
    private static String readSingleLineRespone() throws IOException {
        InputStream inputStream = null;
        if (httpConn != null) {
            inputStream = httpConn.getInputStream();
        } else {
            throw new IOException("Connection is not established.");
        }
        String response;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream))) {
            response = reader.readLine();
        }
 
        return response;
    }
 
    /**
     * Returns an array of lines from the server's response. This method should
     * be used if the server returns multiple lines of String.
     * 
     * @return an array of Strings of the server's response
     * @throws IOException
     *             thrown if any I/O error occurred
     */
    public static String[] readMultipleLinesRespone() throws IOException {
        InputStream inputStream = null;
        if (httpConn != null) {
            inputStream = httpConn.getInputStream();
        } else {
            throw new IOException("Connection is not established.");
        }
 
        List<String> response;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream))) {
            response = new ArrayList<>();
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.add(line);
            }
        }
 
        return (String[]) response.toArray(new String[0]);
    }
    
    /**
     * Closes the connection if opened
     */
    public static void disconnect() {
        if (httpConn != null) {
            httpConn.disconnect();
        }
    }
}
