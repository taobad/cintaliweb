package com.cinttali.utility;

public class OrderResponse {
	
	String responseCode;
	String userStatus;
	String orderStatus;
	ResponseCode responseMessage;
	
	public OrderResponse(){
		
	}
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public ResponseCode getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(ResponseCode responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	

}
