package com.cinttali.utility;

/**
 * Created by buls on 5/30/15.
 */
public enum UserStatus {

    rsv("s"), //registered, synced and verified
    rnn("n"), //registered, not synced, not verified
    rsn("x"), //registered, synced, not verified
    rnv("y"), //registered, not synced, verified
    nsn("h"), //not registered, synced, not verified
    nsv("i"), //not registered, synced, verified
    nnn("g"); //not registered, not synced, not verified


    private String value;
    private UserStatus(String status){
        this.value = status;
    }

}
