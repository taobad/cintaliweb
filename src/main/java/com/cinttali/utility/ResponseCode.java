package com.cinttali.utility;

public enum ResponseCode {

	success("200"),
	failed_bounced("300"),
	internalError("500");
	
	private String value;
    private ResponseCode (String status){
        this.value = status;
    }
    
}
