package com.cinttali.utility;

public enum OrderStatus {

	bounced("b"), //order rejected
	pending("p"), //order owned by unverified account
	received("r"), //server has successfully gotten the order
	seen("s"), //administrator has seen this order
	processing("pr"), //order is being processed
	delivered("d"); //user has received order
	
	private String value;
    private OrderStatus(String status){
        this.value = status;
    }
    
}
