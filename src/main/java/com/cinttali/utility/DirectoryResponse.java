package com.cinttali.utility;

import java.util.ArrayList;
import java.util.List;

public class DirectoryResponse {
	
	private List<Object> payload;
	private List<Object> objectPayload; // to hold created objects. The corresponding create entry in payload should point to created object's index
	private String status;
	private String statusMessage;
	
	public DirectoryResponse(){
		payload = new ArrayList<Object>();
		status = "";
		statusMessage = "";
	}	
	
	public List<Object> getPayload() {
		return payload;
	}

	public void setPayload(List<Object> payload) {
		this.payload = payload;
	}
	
	public List<Object> getObjectPayload() {
		return objectPayload;
	}

	public void setObjectPayload(List<Object> objectPayload) {
		this.objectPayload = objectPayload;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	
	

}
