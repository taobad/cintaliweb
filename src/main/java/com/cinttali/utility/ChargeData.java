package com.cinttali.utility;

public class ChargeData {

	private String TOKEN_URL = "https://api.paystack.co/transaction/charge_token";
	private String AUTH_URL = "https://api.paystack.co/transaction/charge_authorization";
	private String URL;
	private String token;
	private String authorization_code;
	private String email;
	private String amount;
	private String reference;
	
	
	public ChargeData(String token, String authorization_code, String email,
			String amount, String reference) {
		this.token = token;
		this.authorization_code = authorization_code;
		this.email = email;
		this.amount = amount;
		this.reference = reference;
	}

	public ChargeData(){
		
	}

	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
		this.URL = TOKEN_URL;
	}


	public String getAuthorization_code() {
		return authorization_code;
	}


	public void setAuthorization_code(String authorization_code) {
		this.authorization_code = authorization_code;
		this.URL = AUTH_URL;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAmount() {
		return amount;
	}


	public void setAmount(String amount) {
		this.amount = amount;
	}


	public String getReference() {
		return reference;
	}


	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}
	
	
	
}
