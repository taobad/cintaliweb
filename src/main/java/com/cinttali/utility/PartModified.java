package com.cinttali.utility;

public class PartModified {

	public static String MENU_ITEM_NAME = "name";
	public static String MENU_ITEM_PRICE = "price";
	public static String MENU_ITEM_CATEGORY = "category";
	public static String MENU_ITEM_PCKDESC = "pckDesc";
	public static String MENU_ITEM_IMAGE_URL = "imageUrl";
	public static String MENU_ITEM_NUTRITION = "nutrition";
	public static String MENU_ITEM_AVAILABILITY = "isAvailable";
	public static String DELIVERY_SCHEDULE_TIME = "time";
	public static String DELIVERY_SCHEDULE_LOPT = "lopt";
	public static String DELIVERY_SCHEDULE_ACTIVITY = "isActive";
	public static String NEW_MENU_ITEM = "nmi";
	public static String REMOVE_MENU_ITEM = "rmi";
	public static String NEW_OUTLET = "nol";
	public static String REMOVE_OUTLET = "rol";
	public static String NEW_OUTLET_CAT = "noc";
	public static String REMOVE_OUTLET_CAT = "roc";
	public static String OUTLET_ACTIVE = "isActive";
	public static String OUTLET_DELIVERY_CHARGE = "deliveryCharge";
	public static String NEW_DELIVERY_SCHEDULE = "nds";
	public static String REMOVE__DELIVERY_SCHEDULE = "rds";
	public static String NEW_MENU_CATEGORY = "nmc";
	public static String NEW_STREET = "nst";
	public static String UPDATE_ACTION = "u";
	public static String REMOVE_ACTION = "r";
	public static String CREATE_ACTION = "c";
	public static String MENU_ITEM_TYPE = "mi";
	public static String OUTLET_TYPE = "o";
	public static String DELIVERY_SCHEDULE_TYPE = "ds";
	public static String MENU_CATEGORY_TYPE = "mc";
	public static String STREET_TYPE = "st";
	public static String OUTLET_CAT_TYPE = "oc";
	
}
