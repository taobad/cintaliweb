package com.cinttali;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.apache.commons.codec.binary.Base64;

import com.cinttali.manager.ItemManager;
import com.cinttali.manager.LocationManager;
import com.cinttali.manager.OrderManager;
import com.cinttali.manager.OutletManager;
import com.cinttali.manager.SmsManager;
import com.cinttali.manager.UpdateManager;
import com.cinttali.manager.UserManager;
import com.cinttali.model.DeliverySchedule;
import com.cinttali.model.Location;
import com.cinttali.model.MenuCategory;
import com.cinttali.model.MenuItem;
import com.cinttali.model.Outlet;
import com.cinttali.model.OutletCategory;
import com.cinttali.model.SmsEmailCode;
import com.cinttali.model.StateLocationHolder;
import com.cinttali.model.Street;
import com.cinttali.model.UpdateShip;
import com.cinttali.utility.DirectoryResponse;
import com.cinttali.utility.JikaDecode;
import com.cinttali.utility.LocationRelatedObjects;
import com.cinttali.utility.OrderResponse;
import com.cinttali.utility.OrderStatus;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

//add code to handle
//create mealcategories and records update and removal(inactivity) consider makig updates like that of delivery schedules below
@SuppressWarnings("serial")
public class Waiter extends HttpServlet {

	Gson gsonCreator;
	ServletContext logger = null;

	@Override
	public void init() throws ServletException {
		logger = getServletContext();
	    gsonCreator = new GsonBuilder().setPrettyPrinting().create();

	}	
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		String action = req.getParameter("action");
		if(action.equals("startedit")){
			List<MenuItem> menuItems = new ItemManager(logger).getMenuItems();
			req.setAttribute("menuitems", menuItems);
			RequestDispatcher rd = req.getRequestDispatcher("itemselector.jsp");
			rd.forward(req, resp);
		} else if(action.equals("create")){
			List<MenuCategory> menuCats = new ItemManager(logger).getCategories();
			req.setAttribute("mcats", menuCats);
			List<Location> locations = new LocationManager().getLocations();
			req.setAttribute("locs", locations);
			List<Outlet> outlets = new OutletManager().getOutlets();
			req.setAttribute("outs", outlets);
			RequestDispatcher rd = req.getRequestDispatcher("create.jsp");
			rd.forward(req, resp);
		} else if(action.equals("createoutlet")){			
			List<Location> locations = new LocationManager().getLocations();
			req.setAttribute("locs", locations);
			List<OutletCategory> oCats = new LocationManager().getOutletCategories();
			req.setAttribute("cats", oCats);
			RequestDispatcher rd = req.getRequestDispatcher("createoutlet.jsp");
			rd.forward(req, resp);
		} else if(action.equals("serve")){
			
			String itemCode = req.getParameter("itemcode");
			String name = req.getParameter("name");
			String price = req.getParameter("price");
		    String category = req.getParameter("category");
		    String pckDesc = req.getParameter("desc");
		    String nutrition = req.getParameter("nutrition");
		    String imageKey = req.getParameter("blob-key");
			//String imageUrl = req.getParameter("");
			System.out.println(itemCode + "abc");
			BlobKey blobKey = new BlobKey(req.getParameter("blob-key"));			
			BlobInfo blobInfo = new BlobInfoFactory().loadBlobInfo(blobKey);
			//BlobstoreInputStream bsi = new BlobstoreInputStream(blobKey);
			//byte[] imageData = new byte[(int) blobInfo.getSize()];
			
			//bsi.read(imageData);
			//String image64 = Base64.encodeBase64String(imageData);
			
			ServingUrlOptions servingUrlOptions = ServingUrlOptions.Builder.withBlobKey(blobKey);
			ImagesService imagesService = ImagesServiceFactory.getImagesService();
			String surl = imagesService.getServingUrl(servingUrlOptions);
			
			//new ItemManager(logger).createMenuItem(itemCode, name, price, category, pckDesc,
				//	nutrition, imageKey, surl);
			
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			//payload.add(image64);
			payload.add(surl);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("buildobjects")){
			new UserManager().createUserOrderId();
			new ItemManager(logger).createCategories();
			new LocationManager().createLocations();
			new LocationManager().createStates();
			new LocationManager().createOutletCategory("tm-gc", "Groceries", "aa217006", "tm");
			new LocationManager().createOutletCategory("tm-sm", "Super Markets", "aaff0000", "tm");
			new LocationManager().createOutletCategory("tm-vt", "Virtual", "aa0000ff", "tm");
			new OutletManager().createOutlets();
		} else if(action.equals("outlets")){
			String loc = req.getParameter("loc");
			List<Outlet> outlets = new OutletManager().getOutlets(loc);
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			//payload.add(image64);
			for(Outlet o : outlets){
				payload.add(o);
			}
			
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("loccats")){
			String loc = req.getParameter("loc");
			List<OutletCategory> outletCats = new LocationManager().getOutletCategories(loc);
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			//payload.add(image64);
			for(OutletCategory oc : outletCats){
				payload.add(oc);
			}
			
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("coutlet")){
			String outletCode = req.getParameter("oc");
			String name = req.getParameter("on");
			String description = req.getParameter("desc");
			String location = req.getParameter("lc");
			String category = req.getParameter("ocat");
			String deliveryCharge = req.getParameter("dc");
			new OutletManager().createOutlet(outletCode, name, description, location, category, deliveryCharge, "", "");
		} else if(action.equals("clocation")){
			String locationCode = req.getParameter("lc");
			String name = req.getParameter("ln");
			String locality = req.getParameter("lt");
			String state = req.getParameter("st");
			new LocationManager().createLocation(locationCode, name, locality,
					state);
		} else if(action.equals("cstreet")){
			String stCode = req.getParameter("sc");
			String street = req.getParameter("st");
			String locationCode = req.getParameter("lc");
			new LocationManager().createStreet(locationCode+"-"+stCode, street, locationCode);
		} else if(action.equals("cdelsch")){
			String dsCode = req.getParameter("dc");
			String day = req.getParameter("d");
			String time = req.getParameter("tm");
			String lopt = req.getParameter("lt");
			String outlet = req.getParameter("ol");
			String location = req.getParameter("lc");
			String active = req.getParameter("ac");
			String edit = req.getParameter("ed");
			
			if(dsCode != null && day != null && time != null &&
					lopt != null && outlet != null && location != null && 
					active != null){
				if(edit == null || edit.equals("")){
					edit = "false";
				}
				new OutletManager().createDeliverySchedule(dsCode, day, time,
						lopt, location, active, edit, outlet);
			}
		} else if(action.equals("cmenucat")){
			String menuCatCode = req.getParameter("mcc");
			String name = req.getParameter("mcn");
			String color = req.getParameter("cl");
			String location = req.getParameter("lc");
			String outlet = req.getParameter("ol");
			
			new ItemManager(logger).createCategory(menuCatCode, name, color, location, outlet);
		} else if(action.equals("coutletcat")){
			String outletCatCode = req.getParameter("occ");
			String name = req.getParameter("ocn");
			String color = req.getParameter("ocl");
			String location = req.getParameter("oclc");			
			
			new LocationManager().createOutletCategory(outletCatCode, name, color, location);
		} else if(action.equals("udelsch")){ //expected that calls here will supply 2 parameters only
			String dsCode = req.getParameter("dc");			
			String time = req.getParameter("tm");
			String lopt = req.getParameter("lt");
			String active = req.getParameter("ac");
			
			if(dsCode != null && time != null && lopt == null && active == null){ //time				
				new OutletManager().updateDeliverySchedule(dsCode, time, "time");
			} else if(dsCode != null && lopt != null && time == null && active == null){ //lopt				
				new OutletManager().updateDeliverySchedule(dsCode, lopt, "lopt");
			} else if(dsCode != null && active != null && lopt == null && time == null){ //active				
				new OutletManager().updateDeliverySchedule(dsCode, active, "active");
			}
		} else if(action.equals("uoutlet")){ //atleast 2 params will be available
			String outletCode = req.getParameter("oc");
			String deliveryCharge = req.getParameter("dc");
			String isActive = req.getParameter("ia");
			String remove = req.getParameter("rm");
			String location = req.getParameter("lc");
			
			if(remove != null && location != null && deliveryCharge == null && isActive == null){// do a remove
				new OutletManager().removeOutlet(outletCode, location);
			} else if(deliveryCharge != null && isActive == null){ //deliverycharge update
				new OutletManager().updateOutletDeliveryChrage(outletCode, deliveryCharge);
			} else if(deliveryCharge == null && isActive != null){ //availability update
				new OutletManager().updateOutletAvailability(outletCode, isActive);
			}
			
		} else if(action.equals("routletcat")){ 
			String outletCatCode = req.getParameter("occ");			
			String location = req.getParameter("lc");
			
			new LocationManager().removeOutletCat(outletCatCode, location);						
		} else if(action.equals("alllocations")){ //get all registered locations
			List<Location> allLocations = new LocationManager().getLocations();
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			payload.add(allLocations);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("allocategories")){ //get all registered locations
			List<OutletCategory> allOCats = new LocationManager().getOutletCategories();
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			payload.add(allOCats);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("mobileinit")){ //called by apps initialising for the first time for a given location
			String deviceId = req.getParameter("deviceid");
			String locationId = req.getParameter("locid");
			String deviceAppVersion = req.getParameter("dav"); //will be used to differentiate the application versions installed
			System.out.println("DEVICE APP VERSION: " + deviceAppVersion);
			//save device ID
			
			//get location related objects
			List<OutletCategory> oCats = new LocationManager().getOutletCategories(locationId);
			List<Outlet> locOutlets = new OutletManager().getOutlets(locationId);
			List<MenuCategory> menuCats = new ItemManager(logger).getCategories(locationId);
			List<DeliverySchedule> delvSchs = new OutletManager().getDeliverySchedules(locationId); 
			List<MenuItem> menuItems = new ItemManager(logger).getMenuItems(locationId);
			List<Street> streets = new LocationManager().getStreets(locationId);
			int version = new UpdateManager().getLatestUpdateVersion(locationId);
			
			LocationRelatedObjects lro = new LocationRelatedObjects();
			lro.setVersion(version);
			lro.setOutletCategories(oCats);
			lro.setOutlets(locOutlets);
			lro.setMenuCats(menuCats);
			lro.setDelvSchs(delvSchs);
			lro.setMenuItems(menuItems);
			lro.setStreets(streets);
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			payload.add(lro);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("update")){
			String locationId = req.getParameter("lc");
			String version = req.getParameter("appdbversion");
			String deviceAppVersion = req.getParameter("dav"); //will be used to differentiate the application versions installed
			String deviceId = req.getParameter("did");
			String outletRatings = req.getParameter("or");
			System.out.println("DEVICE APP VERSION: " + deviceAppVersion);
			System.out.println("Outlet Ratings: " + outletRatings + " by: " + deviceId);
			//update the outlet ratings if any
			if(outletRatings != null && !outletRatings.equals("")){
				String[] outletRatingParts = outletRatings.split("\\;");
				OutletManager om = new OutletManager();
				for(int i = 0; i < outletRatingParts.length; i++){
					String[] ratingParts = outletRatingParts[i].split("\\|");
					om.rateOutlet(deviceId, ratingParts[0], ratingParts[1]);
				}
			}
			//fetch the update for this location
			UpdateShip us = new UpdateManager().getUpdate(locationId, version);
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			if(us.getUpdates().size() > 0 || us.getCreateObjects().size() > 0 ||
					us.getOutletRatings().size() > 0){
				payload.add(us);
				dr.setPayload(payload);
				dr.setStatus("ok");
				dr.setStatusMessage("ok");	
			} else{				
				dr.setPayload(payload);
				dr.setStatus("failed");
				dr.setStatusMessage("failed");
			}
			
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("getabujastatelocations")){//used to load the initial choose location screen on mobile, so we fetch both states and abuja's locations all at once			
		    StateLocationHolder slh = new LocationManager().getStateLocations("ab");
		    DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
		    payload.add(slh);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("getstatelocations")){
			String stateCode = req.getParameter("state");
			String deviceAppVersion = req.getParameter("dav"); //will be used to differentiate the application versions installed
			System.out.println("DEVICE APP VERSION: " + deviceAppVersion);
			StateLocationHolder slh = new LocationManager().getStateLocations(stateCode);
			DirectoryResponse dr = new DirectoryResponse();			
			List<Object> payload = new ArrayList<Object>();
		    payload.add(slh);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("getstates")){
			String deviceAppVersion = req.getParameter("dav"); //will be used to differentiate the application versions installed
			System.out.println("DEVICE APP VERSION: " + deviceAppVersion);
			StateLocationHolder slh = new LocationManager().getAllStates();
			DirectoryResponse dr = new DirectoryResponse();			
			List<Object> payload = new ArrayList<Object>();
		    payload.add(slh);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("disassociateoutlet")){
			String outletCode = req.getParameter("oc");
			String locationCode = req.getParameter("lc");
			new LocationManager().disassociateOutlet(locationCode, outletCode);
		} else if(action.equals("rate")){
			String deviceId = req.getParameter("did");
			String outletCode = req.getParameter("oc");
			String rating = req.getParameter("rt");
			new OutletManager().rateOutlet(deviceId, outletCode, rating);
		} else if(action.equals("cnfmsms")){
			String email = req.getParameter("e");
			String userCode = req.getParameter("uc");
			SmsEmailCode sec = new SmsManager().confirmSmsCode(email, userCode.trim(), getServletContext());
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			payload.add(sec);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("cnfmemail")){
			String encodedEmail = req.getParameter("e");
			String email = new JikaDecode().decode(encodedEmail, 0);
			SmsEmailCode sec = new SmsManager().confirmEmail(email, getServletContext());
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			payload.add(sec);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
			//send user to confirmation page instead of json response
		} else if(action.equals("appcnfmemail")){ //called by mobile app to check that email has been confirmed
			String email = req.getParameter("e");			
			SmsEmailCode sec = new SmsManager().appConfirmEmail(email, getServletContext());
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			payload.add(sec);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		} else if(action.equals("orderstatus")){
			String email = req.getParameter("e");
			OrderResponse or = new OrderManager().getOrderStatus(email);
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			payload.add(or);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		}
		//resp.setContentType("text/plain");
		//
	}




public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws IOException, ServletException {

	String action = req.getParameter("action");
	
	if(action.equals("si")){
		System.out.println("action seen");
		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		Map<String, BlobKey> blobs = blobstoreService.getUploadedBlobs(req);
		BlobKey blobK = blobs.get("myFile");
	
		String itemCode = req.getParameter("itemcode");
		String name = req.getParameter("name");
		String price = req.getParameter("price");
	    String category = req.getParameter("category");
	    String pckDesc = req.getParameter("desc");
	    String nutrition = req.getParameter("nutrition");
	    String location = req.getParameter("location");
	    String isAvail = req.getParameter("avail");
	    String remove = req.getParameter("remove");
	    String outlet = req.getParameter("outlet");
		String surl = "";
		String itemCodeEdit = "";
		String edit = req.getParameter("edit"); //used to determine if we are editing or creating
		if(edit == null){
			edit = "false";
		} else{
			itemCodeEdit = req.getParameter("itemcodeedit");
			itemCode = itemCodeEdit; //see line 149 or where createmenuitem is called in else
			System.out.println("ITEM CODE EDIT: " + itemCodeEdit);
		}
		
		if (blobK == null) {
		    //we've got no image
			System.out.println("Editing?: " + edit);
			new ItemManager(logger).createMenuItem(itemCodeEdit.trim(), name.trim(), price.trim(), category.trim(), pckDesc.trim(),
					nutrition.trim(), location.trim(), "", surl, edit, isAvail, remove, outlet.trim());
		} else {
			//BlobKey blobKey = new BlobKey(req.getParameter("blob-key"));		   
			//BlobInfo blobInfo = new BlobInfoFactory().loadBlobInfo(blobKey);			
			//BlobstoreInputStream bsi = new BlobstoreInputStream(blobKey);
			//byte[] imageData = new byte[(int) blobInfo.getSize()];			
			//bsi.read(imageData);
			//String image64 = Base64.encodeBase64String(imageData);
			
			try{
				ServingUrlOptions servingUrlOptions = ServingUrlOptions.Builder.withBlobKey(blobK);
				ImagesService imagesService = ImagesServiceFactory.getImagesService();
				surl = imagesService.getServingUrl(servingUrlOptions);
				if(edit != null && edit.equals("true")){//we are editing a picture, so no need to add the locationcode as done when creating new menu items
					new ItemManager(logger).createMenuItem(itemCode.trim(), name.trim(), price.trim(), category.trim(), pckDesc.trim(),
					    	nutrition.trim(), location.trim(), blobK.getKeyString(), surl, edit, isAvail, remove, outlet.trim());
				} else{
				    new ItemManager(logger).createMenuItem(outlet+"-"+itemCode.trim(), name.trim(), price.trim(), category.trim(), pckDesc.trim(),
					    	nutrition.trim(), location.trim(), blobK.getKeyString(), surl, edit, isAvail, remove, outlet.trim());
				}
			} catch(Exception e){
				if(edit != null && edit.equals("true")){ //edit and not changing a photo [give more thought on this]
					new ItemManager(logger).createMenuItem(itemCode.trim(), name.trim(), price.trim(), category.trim(), pckDesc.trim(),
							nutrition.trim(), location.trim(), "", surl, edit, isAvail, remove, outlet.trim());
				} else{
					new ItemManager(logger).createMenuItem(outlet+"-"+itemCode.trim(), name.trim(), price.trim(), category.trim(), pckDesc.trim(),
							nutrition.trim(), location.trim(), "", surl, edit, isAvail, remove, outlet.trim());
				}
				
			}
			//payload.add(image64);			
		    //resp.sendRedirect("/serve?action=serve&blob-key=" + blobK.getKeyString());
		}
		
		DirectoryResponse dr = new DirectoryResponse();
		List<Object> payload = new ArrayList<Object>();
		payload.add(surl);
		dr.setPayload(payload);
		dr.setStatus("ok");
		dr.setStatusMessage("ok");
		resp.getWriter().println(gsonCreator.toJson(dr));
		
		
	} else if(action.equals("editmi")){
		String itemCode = req.getParameter("mi");
		System.out.println("MI: " + itemCode);
		MenuItem menuItem = new ItemManager(logger).getMenuItem(itemCode);
		req.setAttribute("mitem", menuItem);
		List<MenuCategory> menuCats = new ItemManager(logger).getCategories();
		req.setAttribute("mcats", menuCats);
		List<Location> locations = new LocationManager().getLocations();
		req.setAttribute("locs", locations);
		List<Outlet> outlets = new OutletManager().getOutlets();
		req.setAttribute("outs", outlets);
		RequestDispatcher rd = req.getRequestDispatcher("create.jsp");
		rd.forward(req, resp);
	} else if(action.equals("coutlet")){
		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		Map<String, BlobKey> blobs = blobstoreService.getUploadedBlobs(req);
		BlobKey blobK = blobs.get("myFile");
		String outletCode = req.getParameter("oc");
		String name = req.getParameter("on");
		String desc = req.getParameter("desc");
		String location = req.getParameter("lc");
		String category = req.getParameter("ocat");
		String deliveryCharge = req.getParameter("dc");
		System.out.println("category: " + category);
		if(!location.equals("na")){
			String surl = "";
			try{
				ServingUrlOptions servingUrlOptions = ServingUrlOptions.Builder.withBlobKey(blobK);
				ImagesService imagesService = ImagesServiceFactory.getImagesService();
				surl = imagesService.getServingUrl(servingUrlOptions);
				new OutletManager().createOutlet(outletCode, name, desc, location, category, deliveryCharge,
						blobK.getKeyString(), surl);
			} catch(Exception e){
				new OutletManager().createOutlet(outletCode, name, desc, location, category, deliveryCharge,
						"", "");	
			}
			
			
			DirectoryResponse dr = new DirectoryResponse();
			List<Object> payload = new ArrayList<Object>();
			payload.add(surl);
			dr.setPayload(payload);
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
			resp.getWriter().println(gsonCreator.toJson(dr));
		}
	} else if(action.equals("placeorder")){
		String jsonOp = req.getParameter("op");
		OrderResponse or = new OrderManager().processOrder(jsonOp, getServletContext(), null);
		DirectoryResponse dr = new DirectoryResponse();
		List<Object> payload = new ArrayList<Object>();
		payload.add(or);
		dr.setPayload(payload);
		dr.setStatus("ok");
		dr.setStatusMessage("ok");
		resp.getWriter().println(gsonCreator.toJson(dr));
	} else if(action.equals("savetoken")){
		String userId = req.getParameter("e");
		String token = req.getParameter("t");
		String last4 = req.getParameter("l");
		boolean saved = new UserManager().saveUserToken(userId, token, last4);
		DirectoryResponse dr = new DirectoryResponse();
		List<Object> payload = new ArrayList<Object>();
		payload.add(saved);
		dr.setPayload(payload);
		if(saved){
			dr.setStatus("ok");
			dr.setStatusMessage("ok");
		} else {
			dr.setStatus("failed");
			dr.setStatusMessage("failed");
		}
		resp.getWriter().println(gsonCreator.toJson(dr));
	}
}

}
