package com.cinttali.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * Created by buls on 6/12/15.
 */
@PersistenceCapable(detachable="true")
public class User {
	
	@PrimaryKey
	@Persistent
	private String email;
	@Persistent
    private String deviceId;
	@Persistent
	private String fullname;
	@Persistent
	private String streetNo;
	@Persistent
	private String street;
	@Persistent
	private String addStreetInfo;
	@Persistent
	private String location;
	@Persistent
	private String phone;
	@Persistent
	private String lon;
	@Persistent
	private String lat;
	@Persistent
	private String syncStatus;
	@Persistent
	private String payOption;
	@Persistent
	private String authCode;
	@Persistent
	private String cardToken;
	@Persistent
	private String last4;

    public User() {
    	
    }

    public User(String deviceId, String fullname, String streetNo, String street,
                String addStreetInfo, String location, String email, String phone, String lon,
                String lat, String syncStatus, String payOption, String deliveryTime, String pOption) {
        this.deviceId = deviceId;
        this.fullname = fullname;
        this.streetNo = streetNo;
        this.street = street;
        this.addStreetInfo = addStreetInfo;
        this.location = location;
        this.email = email;
        this.phone = phone;
        this.lon = lon;
        this.lat = lat;
        this.syncStatus = syncStatus;
        this.payOption = pOption;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAddStreetInfo() {
        return addStreetInfo;
    }

    public void setAddStreetInfo(String addStreetInfo) {
        this.addStreetInfo = addStreetInfo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

	public String getPayOption() {
		return payOption;
	}

	public void setPayOption(String payOption) {
		this.payOption = payOption;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getCardToken() {
		return cardToken;
	}

	public void setCardToken(String cardToken) {
		this.cardToken = cardToken;
	}

	public String getLast4() {
		return last4;
	}

	public void setLast4(String last4) {
		this.last4 = last4;
	}	
    

}
