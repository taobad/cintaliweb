package com.cinttali.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable(detachable="true")
public class UserOrderId {
	
	@PrimaryKey
	@Persistent
	private String email;		
	@Persistent
	private Long latestOrderId; //always holds the orderId of the latest "Order" placed 
	@Persistent
	private List<Long> orderIds; //holds "CompletedOrder" Ids
		
	public UserOrderId() {
		super();
		orderIds = new ArrayList<Long>();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getLatestOrderId() {
		return latestOrderId;
	}

	public void setLatestOrderId(Long latestOrderId) {
		this.latestOrderId = latestOrderId;
	}

	public List<Long> getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(List<Long> orderIds) {
		this.orderIds = orderIds;
	}
		

}
