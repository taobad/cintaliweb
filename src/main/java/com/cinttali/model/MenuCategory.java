package com.cinttali.model;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class MenuCategory implements Serializable{

	@PrimaryKey
	@Persistent
	private String catCode;	
	@Persistent
	private String name;
	@Persistent
	private String color;
	@Persistent
	private String location;
	@Persistent
	private String outlet;
	
	
	public MenuCategory(){
		
	}
	

	public MenuCategory(String catCode, String name, String color, String location,
			String outlet) {
		super();
		this.catCode = catCode;
		this.name = name;
		this.color = color;
		this.location = location;
		this.outlet = outlet;
	}



	public String getCatCode() {
		return catCode;
	}


	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}

	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getOutlet() {
		return outlet;
	}


	public void setOutlet(String outlet) {
		this.outlet = outlet;
	}

		
}
