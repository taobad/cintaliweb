package com.cinttali.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UpdateShip {
	
	int latestVersion;
	List<Update> updates;
	List<Object> createObjects;
    List<String> outletRatings;
    List<MenuItem> createMenuItems;
    List<Outlet> createOutlets;
    List<OutletCategory> createOutletCategories;
    List<DeliverySchedule> createDelvSchedules;
    List<Street> createStreets;
    List<MenuCategory> createMenuCats;
    
	
	public UpdateShip(){
		updates = new ArrayList<Update>();
		createObjects = new ArrayList<Object>();
		outletRatings = new ArrayList<String>();
		createMenuItems = new ArrayList<MenuItem>();
		createOutlets = new ArrayList<Outlet>();
		createOutletCategories = new ArrayList<OutletCategory>();
		createDelvSchedules = new ArrayList<DeliverySchedule>();
		createStreets = new ArrayList<Street>();
		createMenuCats = new ArrayList<MenuCategory>();
	}
	
	
	
	public int getLatestVersion() {
		return latestVersion;
	}



	public void setLatestVersion(int latestVersion) {
		this.latestVersion = latestVersion;
	}



	public List<Update> getUpdates() {
		return updates;
	}
	public void setUpdates(Update update) {
		this.updates.add(update);
	}
	public List<Object> getCreateObjects() {
		return createObjects;
	}
	public void setCreateObjects(Object createObject) {
		this.createObjects.add(createObject);
	}

	public List<String> getOutletRatings() {
		return outletRatings;
	}

	public void setOutletRatings(String outletCode, String rating) {	
		this.outletRatings.add(outletCode+"!"+rating);
	}



	public List<MenuItem> getCreateMenuItems() {
		return createMenuItems;
	}



	public void setCreateMenuItem(MenuItem createMenuItem) {
		this.createMenuItems.add(createMenuItem);
	}



	public List<Outlet> getCreateOutlets() {
		return createOutlets;
	}



	public void setCreateOutlet(Outlet createOutlet) {
		this.createOutlets.add(createOutlet);
	}



	public List<OutletCategory> getCreateOutletCategories() {
		return createOutletCategories;
	}



	public void setCreateOutletCategory(
			OutletCategory createOutletCategorie) {
		this.createOutletCategories = createOutletCategories;
	}



	public List<DeliverySchedule> getCreateDelvSchedules() {
		return createDelvSchedules;
	}



	public void setCreateDelvSchedule(DeliverySchedule createDelvSchedule) {
		this.createDelvSchedules.add(createDelvSchedule);
	}



	public List<Street> getCreateStreets() {
		return createStreets;
	}



	public void setCreateStreet(Street Street) {
		this.createStreets.add(Street);
	}
	
	public List<MenuCategory> getCreateMenuCats() {
		return createMenuCats;
	}



	public void setCreateMenuCat(MenuCategory mc) {
		this.createMenuCats.add(mc);
	}
	
	

}
