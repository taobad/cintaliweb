package com.cinttali.model;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class DeliverySchedule {
	
	@PrimaryKey
	@Persistent
	private String dsCode;	
	@Persistent
	private String day;
	@Persistent
	private String time;
	@Persistent
	private String lopt;
	@Persistent
	private String outlet;
	@Persistent
	private String location;
	@Persistent
	private boolean isActive;
	
	
	
	public DeliverySchedule() {
		
	}



	public DeliverySchedule(String dsCode, String day, String time,
			String lopt, String location, boolean isActive, String outlet) {
		super();
		this.dsCode = dsCode;
		this.day = day;
		this.time = time;
		this.lopt = lopt;
		this.outlet = outlet;
		this.location = location;
		this.isActive = isActive;
	}
	
	



	public String getDsCode() {
		return dsCode;
	}



	public void setDsCode(String dsCode) {
		this.dsCode = dsCode;
	}



	public String getDay() {
		return day;
	}



	public void setDay(String day) {
		this.day = day;
	}



	public String getTime() {
		return time;
	}



	public void setTime(String time) {
		this.time = time;
	}



	public String getLopt() {
		return lopt;
	}



	public void setLopt(String lopt) {
		this.lopt = lopt;
	}



	public String getOutlet() {
		return outlet;
	}



	public void setOutlet(String outlet) {
		this.outlet = outlet;
	}



	public String getLocation() {
		return location;
	}



	public void setLocation(String location) {
		this.location = location;
	}



	public boolean isActive() {
		return isActive;
	}



	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	

	
	
}
