package com.cinttali.model;

import java.util.ArrayList;
import java.util.List;

public class StateLocationHolder {
	
	private List<State> states;
	private List<Location> locations;
	
	public StateLocationHolder(){
		this.states = new ArrayList<State>();
		this.locations = new ArrayList<Location>();
	}

	public StateLocationHolder(List<State> states, List<Location> locations) {
		super();
		this.states = states;
		this.locations = locations;
	}

	public List<State> getStates() {
		return states;
	}

	public void setState(State state) {
		this.states.add(state);
	}
	
	public void setStates(List<State> states) {
		this.states = states;
	}

	public List<Location> getLocations() {
		return locations;
	}

	public void setLocations(Location location) {
		this.locations.add(location);
	}

	public void setLocations(List<Location> location) {
		this.locations = location;
	}
	
}
