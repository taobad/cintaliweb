package com.cinttali.model;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;


@PersistenceCapable(detachable="true")
public class MenuItem implements Serializable{
//@Persistent(valueStrategy = IdGeneratorStrategy.SEQUENCE)
	@PrimaryKey
	@Persistent
	private String itemCode;	
	@Persistent
	private String name;
	@Persistent
	private int price;
	@Persistent
	private String category;
	@Persistent
	private String pckDesc;	
	@Persistent
	private String nutrition;
	@Persistent
	private String location;
	@Persistent
	private String imageKey;
	@Persistent
	private String imageUrl;
	@Persistent
	private boolean isAvailable;
	@Persistent
	private String outlet;
	
	
	public MenuItem(){
		
	}
	
	public MenuItem(String itemCode, String name, int price, String category,String pckDesc,
			String nutrition, String location, String imageKey, String imageUrl, boolean isAvailable,
			String outlet) {
		super();
		this.itemCode = itemCode; //so mobile devices can store all items it has ever downloaded
		this.name = name;
		this.price = price;
		this.category = category;
		this.pckDesc = pckDesc;
		this.nutrition = nutrition;
		this.location = location;
		this.imageKey = imageKey;
		this.imageUrl = imageUrl;
		this.isAvailable = isAvailable;
		this.outlet = outlet;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPckDesc() {
		return pckDesc;
	}
	public void setPckDesc(String pckDesc) {
		this.pckDesc = pckDesc;
	}
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNutrition() {
		return nutrition;
	}
	public void setNutrition(String nutrition) {
		this.nutrition = nutrition;
	}
	public String getImageKey() {
		return imageKey;
	}
	public void setImageKey(String imageKey) {
		this.imageKey = imageKey;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public String getOutlet() {
		return outlet;
	}

	public void setOutlet(String outlet) {
		this.outlet = outlet;
	}
	
	
}
