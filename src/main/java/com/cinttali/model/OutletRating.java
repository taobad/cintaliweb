package com.cinttali.model;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class OutletRating implements Serializable{

	@PrimaryKey
	@Persistent
	private String ratingId;
	@Persistent
	private String outletCode;
	@Persistent
	private int rating;
	
	public OutletRating(){
		
	}

	
	public OutletRating(String deviceId, String outletCode, int rating) {
		super();
		this.ratingId = deviceId+outletCode;
		this.outletCode = outletCode;
		this.rating = rating;
	}


	public String getRatingId() {
		return ratingId;
	}

	public void setRatingId(String deviceId, String outletCode) {
		this.ratingId = deviceId + outletCode;
	}

	public String getOutletCode() {
		return outletCode;
	}

	public void setOutletCode(String outletCode) {
		this.outletCode = outletCode;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
		
		
}
