package com.cinttali.model;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class OutletCategory implements Serializable{

	@PrimaryKey
	@Persistent
	private String catCode;	
	@Persistent
	private String name;
	@Persistent
	private String color;
	@Persistent
	private String location;
	
	
	public OutletCategory(){
		
	}
	

	public OutletCategory(String catCode, String name, String color, String location) {
		super();
		this.catCode = catCode;
		this.name = name;
		this.color = color;
		this.location = location;
	}



	public String getCatCode() {
		return catCode;
	}


	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}

	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}

}
