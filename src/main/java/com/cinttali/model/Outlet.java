package com.cinttali.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class Outlet {
	
	@PrimaryKey
	@Persistent
	private String outletCode;	
	@Persistent
	private String name;
	@Persistent
	private String description;
	@Persistent
	private String location;
	@Persistent
	private String category;
	@Persistent
	private int deliveryCharge;
	@Persistent
	private String imageKey;
	@Persistent
	private String imageUrl;
	@Persistent
	private boolean isActive;
	@Persistent
	private String lng;
	@Persistent
	private String lat;
	@Persistent
	private String rating;
	
	
	public Outlet() {
		this.isActive = true;
		this.lng = "0.0";
		this.lat = "0.0";
		this.rating = "0|0"; //rating|no of raters
	}



	public Outlet(String outletCode, String name, String description, 
			String location, String category, int deliveryCharge,
			String imageKey, String imageUrl, String rating) {
		super();
		this.outletCode = outletCode;
		this.name = name;
		this.description = description;
		this.location = location;
		this.category = category;
		this.deliveryCharge = deliveryCharge;
		this.imageKey = imageKey;
		this.imageUrl = imageUrl;
		this.isActive = true;
		this.lng = "0.0";
		this.lat = "0.0";
		this.rating = rating;

	}



	public String getOutletCode() {
		return outletCode;
	}



	public void setOutletCode(String outletCode) {
		this.outletCode = outletCode;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getRating() {
		return rating;
	}



	public void setRating(String rating) {
		this.rating = rating;
	}



	public String getLocation() {
		return location;
	}



	public void setLocation(String location) {
		this.location = location;
	}



	public String getCategory() {
		return category;
	}



	public void setCategory(String category) {
		this.category = category;
	}



	public int getDeliveryCharge() {
		return deliveryCharge;
	}



	public void setDeliveryCharge(int deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}



	public String getImageKey() {
		return imageKey;
	}



	public void setImageKey(String imageKey) {
		this.imageKey = imageKey;
	}



	public String getImageUrl() {
		return imageUrl;
	}



	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}



	public boolean isActive() {
		return isActive;
	}


	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}



	public String getLng() {
		return lng;
	}



	public void setLng(String lng) {
		this.lng = lng;
	}



	public String getLat() {
		return lat;
	}


	public void setLat(String lat) {
		this.lat = lat;
	}
	
	

}
