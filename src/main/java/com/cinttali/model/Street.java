package com.cinttali.model;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class Street implements Serializable{

	@PrimaryKey
	@Persistent
	private String streetCode;	
	@Persistent
	private String name;
	@Persistent
	private String location;
	@Persistent
	private String coordinates;
	
	
	
	
	public Street(){
		
	}
	

	public Street(String streetCode, String name, String location) {
		super();
		this.streetCode = streetCode;
		this.name = name;		
		this.location = location;
		this.coordinates = "";
	}


	public String getStreetCode() {
		return streetCode;
	}


	public void setStreetCode(String streetCode) {
		this.streetCode = streetCode;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getCoordinates() {
		return coordinates;
	}


	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}
	
		
}
