package com.cinttali.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.cinttali.utility.PMF;


@PersistenceCapable(detachable="true")
public class Update implements Serializable{

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.SEQUENCE)	
	private Long pk;	
	@Persistent	
	private int version;
	@Persistent
	private String itemCode;
	@Persistent
	private String partModified;
	@Persistent
	private String details;
	@Persistent
	private String action;
	@Persistent
	private String location;
	@Persistent
	private String type;
	
	
	public Update(int lv){
		this.version = lv+1;
	}
	
	
	public Update(){
		
	}


	public Update(int version, String itemCode, String partModified,
			String details, String action, String location, String type) {
		super();
		this.version = version;
		this.itemCode = itemCode;
		this.partModified = partModified;
		this.details = details;
		this.action = action;
		this.location = location;
		this.type = type;
	}

	

	public int getVersion() {
		return version;
	}


	public void setVersion(int version) {
		this.version = version;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public String getPartModified() {
		return partModified;
	}


	public void setPartModified(String partModified) {
		this.partModified = partModified;
	}


	public String getDetails() {
		return details;
	}


	public void setDetails(String details) {
		this.details = details;
	}


	public String getAction() {
		return action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}
	
		
}
