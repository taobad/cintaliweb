package com.cinttali.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class Location implements Serializable{

	@PrimaryKey
	@Persistent
	private String locationCode;	
	@Persistent
	private String name;
	@Persistent
	private String locality;
	@Persistent
	private String state;
	@Persistent
	private String bounds;
	@Persistent
	private List<String> outlets;
	
	
	
	public Location(){
		
	}
	

	public Location(String locationCode, String name, String locality,
			String state) {
		super();
		this.locationCode = locationCode;
		this.name = name;		
		this.locality = locality;
		this.state = state;
		this.bounds = "";
		this.outlets = new ArrayList<String>();
	}


	public String getLocationCode() {
		return locationCode;
	}


	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLocality() {
		return locality;
	}


	public void setLocality(String locality) {
		this.locality = locality;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getBounds() {
		return bounds;
	}


	public void setBounds(String bounds) {
		this.bounds = bounds;
	}


	public List<String> getOutlets() {
		return outlets;
	}


	public void setOutlet(String outlet) {
		if(this.outlets == null){
			this.outlets = new ArrayList<String>();
		}
		this.outlets.add(outlet);
	}
	
		
}
