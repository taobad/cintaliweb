package com.cinttali.model;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class SmsEmailCode implements Serializable{

	@PrimaryKey
	@Persistent
	private String email;	
	@Persistent
	private int code;
	@Persistent
	private boolean emailConfirmed;
	
	public SmsEmailCode(){
		
	}
	

	public SmsEmailCode(String email, int code) {
		super();
		this.email = email;
		this.code = code;
		this.emailConfirmed = false;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getCode() {
		return code;
	}


	public void setCode(int code) {
		this.code = code;
	}


	public boolean isEmailConfirmed() {
		return emailConfirmed;
	}


	public void setEmailConfirmed(boolean emailConfirmed) {
		this.emailConfirmed = emailConfirmed;
	}

	
	
}
