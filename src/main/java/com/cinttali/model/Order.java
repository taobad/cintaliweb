package com.cinttali.model;

import java.util.Date;
import java.util.HashMap;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable(detachable="true")
public class Order {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key orderId;	
	@Persistent
	private String email;
	@Persistent
	private HashMap<String, Integer> items;
	@Persistent
	private HashMap<String, Double> itemCosts;
	@Persistent
	private String deliveryDay;
	@Persistent
	private String deliveryTime;
	@Persistent
	private String payOption;
	@Persistent
	private String state;
	@Persistent
	private Date deliveryDate;
	@Persistent
	private String outletCode;
	@Persistent
	private String locationCode;
	@Persistent
	private Double deliveryCharge;
	@Persistent
	private String reference;
	
	public Order() {
		super();
	}
	
	public Order(String email, HashMap<String, Integer> items, String state) {
		super();
		this.email = email;
		this.items = items;
		this.state = state;
	}
	
	public Order(String email, String state) {
		super();
		this.email = email;
		this.items = new HashMap<String, Integer>();
		this.state = state;
	}

	public Key getOrderId() {
		return orderId;
	}

	public void setOrderId(Key orderId) {
		this.orderId = orderId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public HashMap<String, Integer> getItems() {
		return items;
	}

	public void setItems(HashMap<String, Integer> items) {
		this.items = items;
	}

	public String getDeliveryDay() {
		return deliveryDay;
	}

	public void setDeliveryDay(String deliveryDay) {
		this.deliveryDay = deliveryDay;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getPayOption() {
		return payOption;
	}

	public void setPayOption(String payOption) {
		this.payOption = payOption;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getOutletCode() {
		return outletCode;
	}

	public void setOutletCode(String outletCode) {
		this.outletCode = outletCode;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public HashMap<String, Double> getItemCosts() {
		return itemCosts;
	}

	public void setItemCosts(HashMap<String, Double> itemCosts) {
		this.itemCosts = itemCosts;
	}

	public Double getDeliveryCharge() {
		return deliveryCharge;
	}

	public void setDeliveryCharge(Double deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
}
