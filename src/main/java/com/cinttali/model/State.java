package com.cinttali.model;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class State implements Serializable{

	@PrimaryKey
	@Persistent
	private String stateCode;	
	@Persistent
	private String name;
	@Persistent
	private String country;
	
	public State(){
		
	}
	

	public State(String stateCode, String name, String country) {
		super();
		this.stateCode = stateCode;
		this.name = name;	
		this.country = country;
	}


	public String getStateCode() {
		return stateCode;
	}


	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}

	
	
		
}
