package com.cinttali.manager;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;

import com.cinttali.model.MenuItem;
import com.cinttali.model.User;
import com.cinttali.utility.JikaEncode;
import com.google.apphosting.api.ApiProxy.OverQuotaException;

public class EmailManager {
	
	public void sendOrderMail(long orderId, User user, HashMap<MenuItem, Integer> orderedItems, ServletContext logger, String totalAmount){
        String color = getRandomColor();
		//String mailBody = messageBody;
		String mailBody = "<!DOCTYPE html> <html lang='en'>   <head> " +
		    "<meta charset='utf-8'>" +	    
		    "</head>" +
		    "<body style='background-color:#fefefe;'>" + "<div style='background-color:#034E88;padding:30px;color:#ffffff' > <h2>Ecwa Wuse 2 Bulletin</h2></div>" +
		    "<div style='background-color:#fefefe;'><br/>" + user.getFullname() + totalAmount + "<br/><br/>Your Order has been received.<br/><br/>" +
		    "<center><div style='padding:30px; background-color:" + color +"'>" +
		    "<a href='http://ecwaw2.appspot.com/'><h1>View Bulletin</h1> </a>" + 
		    "</div></center><br/><br/><a href='https://play.google.com/store/apps/details?id=com.ew2bv' ><img src='http://ecwaw2.appspot.com/images/Small.png' alt='Get ECWA W2 App from the Play Store' /></a><br/><br/>";
		    //String mailFooter1 = "<span style='font-size: x-small'><em>- To stop receiving mails click <a href='http://ecwaw2.appspot.com/pub?action=optout&e="; 		    
		    //String mailFooter1a = "'>here</a></em><br/>";  
		    //String mailFooter2 = "<em>- If you've opted-out and wish to start receiving mails again, please click <a href='http://ecwaw2.appspot.com/pub?action=optin&e=";
		    //String mailFooter2a = "'>here</a></em></span><br/><br/>"; 
		    //String mailEnd = "</div></body></html>";
		
		Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        
    	
        MimeBodyPart htmlPart = new MimeBodyPart();
       
        
        
        
        JikaEncode je = new JikaEncode();
        String completeBody = "";
        
    	
	        try{
	        	Message msg = new MimeMessage(session);
	            Multipart mp = new MimeMultipart();
	            completeBody = mailBody + "<br/><br/><br/>" + je.encode(user.getEmail());
	        	htmlPart.setContent(completeBody, "text/html");
	            mp.addBodyPart(htmlPart);
	            msg.setSubject("Cintali Order");
	            msg.setFrom(new InternetAddress("orders@cinta.li", "Cintali Orders"));
	            msg.addRecipient(Message.RecipientType.TO,
	                             new InternetAddress(user.getEmail(), user.getFullname()));	            
	            //msg.setText(messageBody);                       	
	            msg.setContent(mp);	            	            
	            Transport.send(msg);
	            //System.out.println(completeBody);
	            completeBody = "";
	    
	        } catch (AddressException e) {
	        	logger.log("order mail addressexception : " + e.getMessage());
	        } catch (MessagingException e) {
	        	logger.log("order mail messageexception : " + e.getMessage());
	        } catch (UnsupportedEncodingException e) {
	        	logger.log("order mail unsupportedencodingexception : " + e.getMessage());
				
			} catch (OverQuotaException oqe){
				logger.log("order mail overquotaexception : " + oqe.getMessage());
			}
        	
        
	}
	
	public void sendVerificationMail(User user, ServletContext logger){
		logger.log("sending mail " + user.getEmail());
        String color = getRandomColor();
		//String mailBody = messageBody;
		String mailBody = "<!DOCTYPE html> <html lang='en'>   <head> " +
		    "<meta charset='utf-8'>" +	    
		    "</head>" +
		    "<body style='background-color:#fefefe;'>" + "<div style='background-color:#034E88;padding:30px;color:#ffffff' > <h2>Ecwa Wuse 2 Bulletin</h2></div>" +
		    "<div style='background-color:#fefefe;'><br/>" + user.getFullname() + "<br/><br/>Your Order has been received.<br/><br/>" +
		    "<center><div style='padding:30px; background-color:" + color +"'>" +
		    "<a href='http://ecwaw2.appspot.com/'><h1>View Bulletin</h1> </a>" + 
		    "</div></center><br/><br/><a href='https://play.google.com/store/apps/details?id=com.ew2bv' ><img src='http://ecwaw2.appspot.com/images/Small.png' alt='Get ECWA W2 App from the Play Store' /></a><br/><br/>";
		    //String mailFooter1 = "<span style='font-size: x-small'><em>- To stop receiving mails click <a href='http://ecwaw2.appspot.com/pub?action=optout&e="; 		    
		    //String mailFooter1a = "'>here</a></em><br/>";  
		    //String mailFooter2 = "<em>- If you've opted-out and wish to start receiving mails again, please click <a href='http://ecwaw2.appspot.com/pub?action=optin&e=";
		    //String mailFooter2a = "'>here</a></em></span><br/><br/>"; 
		    //String mailEnd = "</div></body></html>";
		
		Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        
    	
        MimeBodyPart htmlPart = new MimeBodyPart();
       
        
        
        
        JikaEncode je = new JikaEncode();
        String completeBody = "";
        
    	
	        try{
	        	Message msg = new MimeMessage(session);
	            Multipart mp = new MimeMultipart();
	            completeBody = mailBody + "<br/><br/><br/><a href=https://cinttali.appspot.com/waiter?action=cnfmemail&e=" + je.encode(user.getEmail()) + ">https://cinttali.appspot.com/waiter?action=cnfmemail&e=" + je.encode(user.getEmail()) + "</a>";
	        	htmlPart.setContent(completeBody, "text/html");
	            mp.addBodyPart(htmlPart);
	            msg.setSubject("Your Cintali Order");
	            msg.setFrom(new InternetAddress("cinttali@appspot.gserviceaccount.com", "Cintali Orders"));
	            msg.addRecipient(Message.RecipientType.TO,
	                             new InternetAddress(user.getEmail(), user.getFullname()));	            
	            //msg.setText(messageBody);                       	
	            msg.setContent(mp);	            	            
	            Transport.send(msg);
	            //System.out.println(completeBody);
	            logger.log("mail sent " + completeBody + msg.getFrom().toString());
	            completeBody = "";
	    
	        } catch (AddressException e) {
	        	logger.log("order mail addressexception : " + e.getMessage());
	        } catch (MessagingException e) {
	        	logger.log("order mail messageexception : " + e.getMessage());
	        } catch (UnsupportedEncodingException e) {
	        	logger.log("order mail unsupportedencodingexception : " + e.getMessage());
				
			} catch (OverQuotaException oqe){
				logger.log("order mail overquotaexception : " + oqe.getMessage());
			} catch (Exception e) {
				logger.log("exception: " + e.getMessage());
			}
        	
        
	}
	
    public String getRandomColor(){
		
		int min = 1;
		int max = 5;
		int pos = (int)(Math.random()*(max-min)+min);
		String[] colors = {"#ff8888", "#88ff88", "#8888ff", "#ffff88", "#ff88ff"};
		
		try{
		    return colors[pos];
		} catch(Exception e){
			return "#ffffff";
		}
		
		
	}

}
