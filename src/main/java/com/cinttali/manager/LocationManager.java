package com.cinttali.manager;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.cinttali.model.DeliverySchedule;
import com.cinttali.model.Location;
import com.cinttali.model.MenuCategory;
import com.cinttali.model.MenuItem;
import com.cinttali.model.OutletCategory;
import com.cinttali.model.State;
import com.cinttali.model.StateLocationHolder;
import com.cinttali.model.Street;
import com.cinttali.model.Update;
import com.cinttali.utility.PMF;
import com.cinttali.utility.PartModified;

public class LocationManager {
	
	public void createLocation(String locationCode, String name, String locality,
			String state){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Location lc = new Location(locationCode, name, locality, state);				
		pm.makePersistent(lc);
		
		pm.close();
	}
	
	

	
	public List<Location> getLocations(){
		List<Location> locations = new ArrayList<Location>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(Location.class);		                
        @SuppressWarnings("unchecked")
		List<Location> ls = (List<Location>)q.execute();        
        locations = ls;        
		pm.close();		
		return locations;		
	}

	public void createLocations() {
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Location lc = new Location("tm", "Trademore Estate", "Lugbe", "ab");
		Location lc1 = new Location("ef", "Efab Estate", "Life Camp", "ab");		
		
		pm.makePersistentAll(lc, lc1);
		
		pm.close();
		
	}
	
    public void createStates() {
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		State st = new State("ab", "Abuja", "ng");
		State st1 = new State("lg", "Lagos", "ng");		
		
		pm.makePersistentAll(st, st1);
		
		pm.close();
		
	}




	public void createStreet(String stCode, String street, String locationCode) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Street st = new Street(stCode, street, locationCode);
		pm.makePersistent(st);
		new UpdateManager().logStreetUpdate(null, st);
		pm.close();
		
	}
	
	public void createOutletCategory(String outletCode, String name, 
			String color, String location) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		OutletCategory oCat = new OutletCategory(outletCode, name,
				color, location);
		pm.makePersistent(oCat);
		new UpdateManager().logOutletCategoryUpdate(null, oCat);
		pm.close();
		
	}
	
	public Location getLocation(String loc) {
		Location location = null;		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		location = pm.detachCopy(pm.getObjectById(Location.class, loc));       
		pm.close();		
		return location;
	}
	
	public List<Street> getStreets(String loc) {
		List<Street> streets = new ArrayList<Street>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(Street.class);
		q.setFilter("location == l");
		q.declareParameters("String l");
        @SuppressWarnings("unchecked")
		List<Street> sts = (List<Street>)q.execute(loc); 
        for(Street st : sts){//detach copies
        	//if(st.getLocation().equals(loc)){
        		streets.add(pm.detachCopy(st));
        	//}
        }
               
		pm.close();		
		return streets;
	}


	public List<OutletCategory> getOutletCategories(String loc) {
		List<OutletCategory> oCategories = new ArrayList<OutletCategory>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(OutletCategory.class);
		q.setFilter("location == l");
		q.declareParameters("String l");
        @SuppressWarnings("unchecked")
		List<OutletCategory> oCats = (List<OutletCategory>)q.execute(loc); 
        for(OutletCategory oc : oCats){//detach copies
        	//if(st.getLocation().equals(loc)){
        		oCategories.add(pm.detachCopy(oc));
        	//}
        }              
		pm.close();		
		return oCategories;
	}
	
	public List<OutletCategory> getOutletCategories() {
		List<OutletCategory> oCategories = new ArrayList<OutletCategory>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(OutletCategory.class);		
        @SuppressWarnings("unchecked")
		List<OutletCategory> oCats = (List<OutletCategory>)q.execute(); 
        for(OutletCategory oc : oCats){//detach copies
        	//if(st.getLocation().equals(loc)){
        		oCategories.add(pm.detachCopy(oc));
        	//}
        }              
		pm.close();		
		return oCategories;
	}


	public Street getStreet(String itemCode) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Street s = pm.detachCopy(pm.getObjectById(Street.class, itemCode));
		pm.close();
		return s;
	}


	public StateLocationHolder getAllStates() {
		StateLocationHolder slh = new StateLocationHolder();
		
		List<State> state = this.getStates();		
		slh.setStates(state);
		
		return slh;
	}

	public StateLocationHolder getStateLocations(String stateCode) {
		StateLocationHolder slh = new StateLocationHolder();
		
		State state = this.getState(stateCode);
		List<Location> locations = this.getLocations(stateCode);
		if(state != null){
			slh.setState(state);	
		}		
		slh.setLocations(locations);
		
		return slh;
	}
	
	public State getState(String stateCode){
		State state = null;
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try{
		    state = pm.detachCopy(pm.getObjectById(State.class, stateCode));
		} catch(Exception e){
			
		}
		return state;
	}
	
	public List<Location> getLocations(String stateCode) {
		List<Location> locations = new ArrayList<Location>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(Location.class);
		q.setFilter("state == s");
		q.declareParameters("String s");
        @SuppressWarnings("unchecked")
		List<Location> locs = (List<Location>)q.execute(stateCode); 
        locations = locs;
               
		pm.close();		
		return locations;
	}
	
	public List<State> getStates() {
		List<State> states = new ArrayList<State>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(State.class);		
		List<State> sts = (List<State>)q.execute(); 
        states = sts;
               
		pm.close();		
		return states;
	}




	public void disassociateOutlet(String locationCode, String outletCode) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Location loc = this.getLocation(locationCode);
		int counter = 0;
		boolean doRemove = false;
		for(String oc : loc.getOutlets()){
			if(oc.equals(outletCode)){
				doRemove = true;
				break;
			}
			counter++;
		}
		if(doRemove){
			loc.getOutlets().remove(counter);
			pm.makePersistent(loc);
		}		
		
		pm.close();
	}

	public void removeOutletCat(String outletCatCode, String location) {
		new UpdateManager().removeOutletCat(outletCatCode, location);
		
	}

		
}
