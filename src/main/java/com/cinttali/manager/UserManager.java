package com.cinttali.manager;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;

import com.cinttali.model.User;
import com.cinttali.model.UserOrderId;
import com.cinttali.utility.PMF;
import com.cinttali.utility.ServiceConstant;

public class UserManager {
	
	public boolean saveUser(User user){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		boolean saved = false;
		if (user.getPayOption().equals(ServiceConstant.PAY_ONLINE_GATEWAY.getValue())) {
			User savedUser = getUser(user.getEmail());
			if (savedUser != null) {				
			    user.setCardToken(savedUser.getCardToken());
			    user.setLast4(savedUser.getLast4());
			    pm.makePersistent(user);
				saved = true;
				pm.close();
			} else {
				return saved;
			}
		} else {
			pm.makePersistent(user);
			saved = true;
			pm.close();
		}
		return saved;
	}
	
	public User getUser(String email){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		User user = null;
		try{
			user = pm.detachCopy(pm.getObjectById(User.class, email));
		} catch(JDOObjectNotFoundException jonfe){
			
		}
		return user;
	}
	
	public void createUserOrderId(){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		UserOrderId uoi = new UserOrderId();
		uoi.setEmail("abc");
		pm.makePersistent(uoi);
		pm.close();
		
	}
	
	public boolean saveUserToken(String userId, String cardToken, String last4){
		boolean saved = false;
		if(userId != null && cardToken != null){
			PersistenceManager pm = PMF.get().getPersistenceManager();
		    User user = getUser(userId);
		    if (user == null) {
		    	user = new User();
		    	user.setEmail(userId);
		    }
		    user.setCardToken(cardToken);
		    user.setLast4(last4);
		    pm.makePersistent(user);
			pm.close();
			saved = true;
		}
		return saved;
	}
	
	public boolean saveUserAuthCode(String userId, String authCode){
		boolean saved = false;
		if(userId != null && authCode != null){
			PersistenceManager pm = PMF.get().getPersistenceManager();
		    User user = getUser(userId);
		    if (user == null) {
		    	return saved;
		    }
		    user.setCardToken("");
		    user.setAuthCode(authCode);
		    pm.makePersistent(user);
			pm.close();
			saved = true;
		}
		return saved;
	}
	
	
}

