package com.cinttali.manager;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletContext;

import com.cinttali.model.MenuCategory;
import com.cinttali.model.MenuItem;
import com.cinttali.model.Outlet;
import com.cinttali.model.Update;
import com.cinttali.utility.PMF;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.cinttali.utility.PartModified;

public class ItemManager {
	ServletContext logger;
	public ItemManager(ServletContext sc){
		logger = sc;
	}
	
	public ItemManager(){
		
	}
	
	public Boolean createMenuItem(String itemCode, String name, String price,
			String category, String pckDesc, String nutrition, String location, String imageKey,
			String imageUrl, String edit, String isAvailable, String remove, String outlet){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Boolean created;
		if(remove == null){ //not doing a remove
		
		MenuItem mi = null;
		boolean isAvail;
		if(isAvailable.equals("true")){
			isAvail = true;
		} else{
			isAvail = false;
		}
		if(edit.equals("true") && imageUrl.equals("")){ //image not modified
			MenuItem oldMi = this.getMenuItem(itemCode);
			mi = new MenuItem(itemCode, name, Integer.parseInt(price), category, pckDesc,
			    	 nutrition, location, oldMi.getImageKey(), oldMi.getImageUrl(), isAvail, outlet);
			new UpdateManager().logMenuItemUpdate(oldMi, mi);
		} else if(edit.equals("true") && !imageUrl.equals("")){ //image modified
			mi = new MenuItem(itemCode, name, Integer.parseInt(price), category, pckDesc,
			    	 nutrition, location, imageKey, imageUrl, isAvail, outlet);
			MenuItem oldMi = this.getMenuItem(itemCode);
			BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
			blobstoreService.delete(new BlobKey(oldMi.getImageKey()));
			new UpdateManager().logMenuItemUpdate(oldMi, mi);
	
	    }else{
		    mi = new MenuItem(itemCode, name, Integer.parseInt(price), category, pckDesc,
			    	 nutrition, location, imageKey, imageUrl, isAvail, outlet);
		    new UpdateManager().logMenuItemUpdate(null, mi);
		}
		pm.makePersistent(mi);
		pm.close();
		
		//notify the users who have a local copy of this location's database here
		} else{ //do a remove
			new UpdateManager().removeMenuItem(itemCode, location);
		}
		created = true;
		return created;
	}

	
	
	
	
	public List<MenuItem> getMenuItems(){
		List<MenuItem> menuItems = new ArrayList<MenuItem>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(MenuItem.class);		                
        @SuppressWarnings("unchecked")
		List<MenuItem> mis = (List<MenuItem>)q.execute();        
        menuItems = mis;        
		pm.close();		
		return menuItems;		
	}

	public MenuItem getMenuItem(String itemCode) {
		MenuItem menuItem = null;
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try{
			if(itemCode != null && !itemCode.equals("")){
				MenuItem mi = pm.getObjectById(MenuItem.class, itemCode);
				menuItem = mi;
				System.out.println("get menu item image key: " + mi.getImageKey());
			}
		} catch(Exception e){
			if(logger != null){
			    logger.log("getMenuItem error: " + e);
			}
			
			return menuItem;
		}
		pm.close();
		
		return menuItem;
	}
	
	

	//used to create initial menu categories
	public void createCategories() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		MenuCategory mc = new MenuCategory("v", "Vegetables", "aa217006", "tm", "gn");
		MenuCategory mc1 = new MenuCategory("f", "Fruits", "aaffff00", "tm", "gn");
		MenuCategory mc2 = new MenuCategory("s", "Spices", "aaffa500", "tm", "gn");
		MenuCategory mc3 = new MenuCategory("p", "Protien", "aaff0000", "tm", "gn");
		
		pm.makePersistentAll(mc, mc1, mc2, mc3);
		
		pm.close();
		
	}

	public List<MenuCategory> getCategories() {
		List<MenuCategory> menuCats = new ArrayList<MenuCategory>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(MenuCategory.class);		                
        @SuppressWarnings("unchecked")
		List<MenuCategory> mc = (List<MenuCategory>)q.execute();        
        menuCats = mc;        
		pm.close();		
		return menuCats;
	}
	
	public List<MenuCategory> getCategories(String loc) {
		List<MenuCategory> menuCats = new ArrayList<MenuCategory>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(MenuCategory.class);
		q.setFilter("location == l");
		q.declareParameters("String l");
        @SuppressWarnings("unchecked")
		List<MenuCategory> mCats = (List<MenuCategory>)q.execute(loc); 
        for(MenuCategory mc : mCats){
        	if(mc.getLocation().equals(loc)){
        		menuCats.add(pm.detachCopy(mc));
        	}
        }
               
		pm.close();		
		return menuCats;
	}

	public List<MenuItem> getMenuItems(String loc) {
		List<MenuItem> menuItems = new ArrayList<MenuItem>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(MenuItem.class);
		q.setFilter("location == l");
		q.declareParameters("String l");
        @SuppressWarnings("unchecked")
		List<MenuItem> mItems = (List<MenuItem>)q.execute(loc); 
        for(MenuItem mi : mItems){
        	if(mi.getLocation().equals(loc)){
        		mi = pm.detachCopy(mi);
        		mi.setImageKey("");        		
        		menuItems.add(mi);
        	}
        }
               
		pm.close();		
		return menuItems;
	}
	
	public void createCategory(String menuCatCode, String name, String color,
			String location, String outlet) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		MenuCategory mc = new MenuCategory(menuCatCode, name, color, location, outlet);		
		pm.makePersistentAll(mc);		
		pm.close();
		new UpdateManager().logMenuCategoryUpdate(null, mc);
	}

	public MenuCategory getCategory(String itemCode) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		MenuCategory mc = pm.detachCopy(pm.getObjectById(MenuCategory.class, itemCode));
		pm.close();
		return mc;
	}

	
	
}
