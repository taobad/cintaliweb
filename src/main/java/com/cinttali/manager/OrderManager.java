package com.cinttali.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.servlet.ServletContext;

import org.json.JSONObject;

import com.cinttali.utility.ChargeData;
import com.cinttali.utility.OrderPackage;
import com.cinttali.utility.OrderResponse;
import com.cinttali.utility.OrderStatus;
import com.cinttali.utility.PMF;
import com.cinttali.utility.ResponseCode;
import com.cinttali.utility.ServiceConstant;
import com.cinttali.model.MenuItem;
import com.cinttali.model.Order;
import com.cinttali.model.SmsEmailCode;
import com.cinttali.model.UnverifiedOrder;
import com.cinttali.model.User;
import com.cinttali.model.UserOrderId;
import com.cinttali.utility.UserStatus;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class OrderManager {

	public OrderResponse processOrder(String jsonOp, ServletContext sc,
			OrderPackage cop) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		OrderPackage op = null;
		if (jsonOp != null) {
			op = new GsonBuilder().create()
					.fromJson(jsonOp, OrderPackage.class);
		} else {
			op = cop;
		}
		OrderResponse or = new OrderResponse();
		String userStatus = op.getUser().getSyncStatus();
		if (userStatus.equals(UserStatus.nnn.toString())) { // guest
			// send mail and text for verification
			sc.log("guest");
			User user = op.getUser();
			user.setSyncStatus(UserStatus.nsn.toString());
			or = saveUnverifiedOrder(user, op, sc, pm);

		} else if (userStatus.equals(UserStatus.rnn.toString())) { // register,
																	// not
																	// synced,
																	// not
																	// verified
			// save record, flag as synced
			User user = op.getUser();
			user.setSyncStatus(UserStatus.rsn.toString());
			sc.log("calling saveunverified");
			or = saveUnverifiedOrder(user, op, sc, pm);

		} else if (userStatus.equals(UserStatus.rsn.toString())) { // register,
																	// synced,
																	// not
																	// verified
			// bounce this order till user has verified his/her email and phone
			// number
			or.setUserStatus(UserStatus.rsn.toString());
			or.setOrderStatus(OrderStatus.bounced.toString());
			or.setResponseCode(ResponseCode.failed_bounced.toString());
			or.setResponseMessage(ResponseCode.failed_bounced);

		} else if (userStatus.equals(UserStatus.rnv.toString())) { // register,
																	// not
																	// synced,
																	// verified
			// save record
			// flag as synced
		} else if (userStatus.equals(UserStatus.nsv.toString())) { // not
																	// registered,
																	// synced,
																	// verified
			// place order
			// create real order and retrieve orderId
			placeOrder(op, or, pm, sc);

		} else if (userStatus.equals(UserStatus.rsv.toString())) { // register,
																	// synced,
																	// verified
			// place order
			// create real order and retrieve orderId
			placeOrder(op, or, pm, sc);

		}

		pm.close();
		return or;
	}

	private OrderResponse placeOrder(OrderPackage op, OrderResponse or,
			PersistenceManager pm, ServletContext sc) {
		User user = op.getUser();
		Order order = new Order();
		order.setEmail(user.getEmail());
		order.setOutletCode(op.getOutletCode());
		order.setLocationCode(user.getLocation());
		order.setItems(op.getOrderItems());
		order.setDeliveryTime(op.getDeliveryTime());

		String deliveryTime = op.getDeliveryTime();
		boolean isPm = false;
		if (deliveryTime.contains("pm")) {
			deliveryTime = deliveryTime.replace("pm", "");
			isPm = true;
		}

		if (deliveryTime.contains("am")) {
			deliveryTime = deliveryTime.replace("am", "");
		}

		String[] timeParts = deliveryTime.split("\\:");
		Integer hours = Integer.parseInt(timeParts[0]);
		Integer minutes = Integer.parseInt(timeParts[1]);
		if (isPm) {
			hours = hours + 12;
		}

		HashMap<String, Double> itemCosts = getItemCosts(op.getOrderItems());
		order.setItemCosts(itemCosts);

		Integer deliveryCharge = new OutletManager().getOutletDeliveryCharge(op
				.getOutletCode());
		order.setDeliveryCharge(deliveryCharge.doubleValue());

		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.set(Calendar.HOUR_OF_DAY, hours);
		c.set(Calendar.MINUTE, minutes);
		order.setDeliveryTime(op.getDeliveryTime());
		order.setDeliveryDate(c.getTime());
		order.setDeliveryDay(c.getDisplayName(Calendar.DAY_OF_WEEK,
				Calendar.LONG, Locale.getDefault()));
		order.setPayOption(user.getPayOption());
		order.setState(OrderStatus.received.toString());

		String orderTotalAmount = "";
		if (order.getPayOption().equals(
				ServiceConstant.PAY_ONLINE_GATEWAY.getValue())) {
			Double serviceCharge = calculateServiceCharge(order.getItemCosts(),
					order.getDeliveryCharge());
			HashMap<String, Double> orderItemCosts = order.getItemCosts();
			orderItemCosts.put(ServiceConstant.SERVICE_CHARGE.getValue(),
					serviceCharge);
			order.setItemCosts(orderItemCosts);
			// charge card, check if it is first time card is being charged or
			// not
			UserManager um = new UserManager();
			User savedUser = um.getUser(user.getEmail());
			orderTotalAmount = computeTotalPrice(orderItemCosts, order.getDeliveryCharge());
			String reference = chargeUser(user.getEmail(), orderTotalAmount, sc);
			if (reference != null && !reference.equals("")) { //payment successful!
				order.setReference(reference);
			} else {
				or.setUserStatus(user.getSyncStatus());
				or.setOrderStatus(order.getState());
				or.setResponseCode(ResponseCode.failed_bounced.toString());
				or.setResponseMessage(ResponseCode.failed_bounced);
				return or;
			}
			
		}
		sc.log(order.getPayOption() + " sc "
				+ ServiceConstant.PAY_ONLINE_GATEWAY);
		order = pm.makePersistent(order);
		// we might also need to keep track of user's orderId by sending
		// it back to user (see order response object below) (create an entity
		// to store user's orderIds with latest orderId)
		UserOrderId uoi = null;
		uoi = getUserOrderId(user.getEmail());
		if (uoi == null) {
			uoi = new UserOrderId();
			uoi.setEmail(user.getEmail());
		}
		uoi.setLatestOrderId(order.getOrderId().getId());
		uoi.getOrderIds().add(order.getOrderId().getId());
		pm.makePersistent(uoi);
		if (user.getSyncStatus().equals(UserStatus.nsv.toString())) {
			pm.makePersistent(user);
		}

		// build the ordered menuItems for emailing
		HashMap orderItems = order.getItems();
		Set<String> keys = orderItems.keySet();
		ItemManager itemManager = new ItemManager();
		HashMap<MenuItem, Integer> orderItemsForMail = new HashMap<MenuItem, Integer>();
		for (String mItemId : keys) {
			MenuItem mi = itemManager.getMenuItem(mItemId);
			orderItemsForMail.put(mi, (Integer) orderItems.get(mItemId));
		}
		// send order received mail/invoice
		new EmailManager().sendOrderMail(order.getOrderId().getId(), user,
				orderItemsForMail, sc, orderTotalAmount);

		or.setUserStatus(user.getSyncStatus());
		or.setOrderStatus(order.getState());
		or.setResponseCode(((Long) order.getOrderId().getId()).toString());
		or.setResponseMessage(ResponseCode.success);

		this.removeUnverifiedOrder(op.getUser().getEmail());
		// sc.log("unverified order removed " + op.getUser().getEmail());
		// send order placed email
		// send order response

		return or;
	}

	private String computeTotalPrice(HashMap<String, Double> orderItemCosts, Double deliveryCharge) {
		String totalAmount = "0";
		Double totalCost = 0.0;
		Set<String> orderItemIds = orderItemCosts.keySet();
		for(String orderItemId : orderItemIds) {
			Double cost = orderItemCosts.get(orderItemId);
			totalCost += cost;
		}
		totalCost += deliveryCharge;
		totalCost = totalCost * 100;
		totalAmount = ((Integer)totalCost.intValue()).toString();
		
		return totalAmount;
	}
	
	private OrderResponse saveUnverifiedOrder(User user, OrderPackage op,
			ServletContext sc, PersistenceManager pm) {
		Logger log = Logger.getLogger(OrderManager.class.getName());
		log.info("Your information log message.");
		sc.log("save unverified called");
		UnverifiedOrder uo = new UnverifiedOrder();
		uo.setEmail(user.getEmail());
		uo.setOutletCode(op.getOutletCode());
		uo.setLocationCode(user.getLocation());
		uo.setItems(op.getOrderItems());
		uo.setDeliveryTime(op.getDeliveryTime());
		sc.log("section 1");

		String deliveryTime = op.getDeliveryTime();
		boolean isPm = false;
		if (deliveryTime.contains("pm")) {
			deliveryTime = deliveryTime.replace("pm", "");
			isPm = true;
		}

		if (deliveryTime.contains("am")) {
			deliveryTime = deliveryTime.replace("am", "");
		}

		String[] timeParts = deliveryTime.split("\\:");
		Integer hours = Integer.parseInt(timeParts[0]);
		Integer minutes = Integer.parseInt(timeParts[1]);
		if (isPm) {
			hours = hours + 12;
		}

		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.set(Calendar.HOUR_OF_DAY, hours);
		c.set(Calendar.MINUTE, minutes);
		uo.setDeliveryTime(op.getDeliveryTime());
		uo.setDeliveryDate(c.getTime());
		uo.setDeliveryDay(c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG,
				Locale.getDefault()));
		uo.setPayOption(user.getPayOption());
		uo.setState(OrderStatus.pending.toString());
		boolean isUserSaved = new UserManager().saveUser(user);
		OrderResponse or = new OrderResponse();
		sc.log("user saved " + isUserSaved);
		if (isUserSaved) {
			pm.makePersistent(uo);
			// send mail and text for verification
			new EmailManager().sendVerificationMail(user, sc);
			new SmsManager().sendSms(user.getPhone(), user.getEmail(), sc);
			
			or.setUserStatus(user.getSyncStatus());
			or.setOrderStatus(uo.getState());
			or.setResponseCode(ResponseCode.success.toString());
			or.setResponseMessage(ResponseCode.success);
		} else {
			or.setUserStatus(user.getSyncStatus());
			or.setOrderStatus(uo.getState());
			or.setResponseCode(ResponseCode.failed_bounced.toString());
			or.setResponseMessage(ResponseCode.failed_bounced);			
		}
		return or;
	}

	private HashMap<String, Double> getItemCosts(HashMap<String, Integer> items) {
		HashMap<String, Double> itemCosts = new HashMap<String, Double>();
		Set<String> keySet = items.keySet();
		ItemManager itemManager = new ItemManager();
		for (String itemCode : keySet) {
			int itemUnitCost = itemManager.getMenuItem(itemCode).getPrice();
			int itemTotalCost = items.get(itemCode) * itemUnitCost;
			itemCosts.put(itemCode, ((Integer) itemTotalCost).doubleValue());
		}
		return itemCosts;
	}

	private Double calculateServiceCharge(HashMap<String, Double> itemCosts,
			Double deliveryCharge) {
		Set<String> itemCodes = itemCosts.keySet();
		Double itemCostTotal = 0.0;
		for (String itemKey : itemCodes) {
			itemCostTotal += itemCosts.get(itemKey);
		}
		itemCostTotal += deliveryCharge;

		Double serviceCharge = itemCostTotal * 0.0194;
		if ((itemCostTotal + serviceCharge) > 2499.0) {
			serviceCharge += 100.0;
		}

		//serviceCharge = round(serviceCharge, 2);

		if (serviceCharge > 2000.0) {
			serviceCharge = 2000.0;
		}
		
		itemCostTotal += serviceCharge;
		
		//refine
		Double paystackCharge = itemCostTotal * 0.019;
		Double flatRate = itemCostTotal - serviceCharge;
		Double paystackRate = paystackCharge / flatRate;
		Double refinedServiceCharge = flatRate * paystackRate;
		
		//do actuals
		itemCostTotal -= serviceCharge;
		if ((itemCostTotal + refinedServiceCharge) > 2499.0) {
			refinedServiceCharge += 100.0;
		}
		if (refinedServiceCharge > 2000.0) {
			refinedServiceCharge = 2000.0;
		}
		
		return serviceCharge;
	}

	public double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	private UserOrderId getUserOrderId(String email) {
		UserOrderId uoi = null;
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			uoi = pm.getObjectById(UserOrderId.class, email);
		} catch (Exception e) {

		}
		if (uoi != null) {
			uoi = pm.detachCopy(uoi);
		}
		pm.close();
		return uoi;
	}

	private void removeUnverifiedOrder(String email) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		UnverifiedOrder uo = null;
		try {
			uo = pm.getObjectById(UnverifiedOrder.class, email);
		} catch (Exception e) {

		}
		if (uo != null) {
			pm.deletePersistent(uo);
		}
		pm.close();

	}

	public void processUnverifiedOrder(SmsEmailCode sec, ServletContext sc) {
		if (sec != null && sec.getCode() == 0 && sec.isEmailConfirmed()) {
			PersistenceManager pm = PMF.get().getPersistenceManager();
			OrderPackage op = new OrderPackage();
			User user = new UserManager().getUser(sec.getEmail());
			UnverifiedOrder uo = getUnverifiedOrder(sec.getEmail());
			if (uo != null) {				
				op.setOrderItems(uo.getItems());
				op.setDeliveryTime(uo.getDeliveryTime());
				op.setOutletCode(uo.getOutletCode());
				if (user.getSyncStatus().equals(UserStatus.nsn.toString())) {
					user.setSyncStatus(UserStatus.nsv.toString());
				} else {
					user.setSyncStatus(UserStatus.rsv.toString());	
				}				
				op.setUser(user);
				pm.makePersistent(user);
				pm.deletePersistent(sec);
				pm.deletePersistent(uo);
				pm.close();
				this.processOrder(null, sc, op);
			}
		}
	}

	private UnverifiedOrder getUnverifiedOrder(String email) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		UnverifiedOrder uo = null;
		try {
			UnverifiedOrder pUo = pm
					.getObjectById(UnverifiedOrder.class, email);
			HashMap items = pUo.getItems();
			uo = pm.detachCopy(pUo);
			uo.setItems(items);
			pm.close();
		} catch (JDOObjectNotFoundException jonfe) {

		}
		return uo;
	}

	public OrderResponse getOrderStatus(String email) {
		OrderResponse or = new OrderResponse();
		UserOrderId uoi = getUserOrderId(email);
		if (uoi != null) {
			Long latestOrderId = uoi.getLatestOrderId();
			if (latestOrderId != null) {
				Order Order = getOrder(latestOrderId);
				String orderStatus = Order.getState();
				or.setUserStatus("");
				or.setOrderStatus(orderStatus);
				or.setResponseCode(latestOrderId.toString());
				or.setResponseMessage(ResponseCode.success);
			}
		}

		return or;
	}

	public Order getOrder(Long orderId) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Order order = null;
		try {
			Key orderKey = KeyFactory.createKey(Order.class.getSimpleName(),
					orderId);
			order = pm.getObjectById(Order.class, orderKey);
			order = pm.detachCopy(order);
			pm.close();
		} catch (JDOObjectNotFoundException jonfe) {

		}
		return order;
	}
	
	public String chargeUser(String userId, String amount, ServletContext sc) {
		String transactionReference = null;
		User user = new UserManager().getUser(userId);
		
		if (user != null) {
			if (user.getAuthCode() == null && user.getCardToken() != null) { //never charged
				ChargeData chargeData = new ChargeData();
				chargeData.setToken(user.getCardToken());
				chargeData.setEmail(user.getEmail());
				chargeData.setAmount(amount);
				//chargeData.setReference(user.getLast4());
				transactionReference = chargeUser(chargeData, sc);
			} else if (user.getAuthCode() != null) {
				ChargeData chargeData = new ChargeData();
				chargeData.setAuthorization_code(user.getAuthCode());
				chargeData.setEmail(user.getEmail());
				chargeData.setAmount(amount);
				//chargeData.setReference(user.getLast4());
				transactionReference = chargeUser(chargeData, sc);
			}
		}
		
		
		return transactionReference;
	}
		
	private String chargeUser(ChargeData chargeData, ServletContext sc) {
		String transactionReference = null;
		Gson gsonCreator = new GsonBuilder().create();
		String jsonChargeData = gsonCreator.toJson(chargeData);
		sc.log("charge data: " + jsonChargeData);
		
        try {
        	String encodedData = URLEncoder.encode(jsonChargeData, "UTF-8");
        	sc.log(jsonChargeData);
            URL url = new URL(chargeData.getURL());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Bearer sk_test_d41b0a9a763b371dac9da6c8d49b7814d0542a13");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestMethod("POST");

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(jsonChargeData);
            writer.close();
    
            StringBuilder sb = new StringBuilder();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            	BufferedReader br = new BufferedReader(
                        new InputStreamReader(connection.getInputStream(), "utf-8"));
                String line = null;  
                while ((line = br.readLine()) != null) {  
                    sb.append(line + "\n");  
                }
                br.close();
                if (sb != null) {
                	JSONObject response = new JSONObject(sb.toString());
                	boolean status = response.optBoolean("status");
                	sc.log(response.toString());
                	sc.log("Status: " + status); 
                	String authCode = null;
                	if(status) {
                		authCode = response.optJSONObject("data")
                				.optJSONObject("authorization")
                				.optString("authorization_code");
                		sc.log("authcode: " + authCode);
                		if (authCode != null) {
                			boolean saved = new UserManager().saveUserAuthCode(chargeData.getEmail(), authCode);
                			sc.log("authcode saved: " + saved);
                			if (saved) {
                			    transactionReference = response.optJSONObject("data")
                        				.optString("reference");
                			} else {
                				//send mail of issue to admin
                			}
                		}
                	}
                }
                sc.log(sb.toString());  
            } else {
            	BufferedReader br = new BufferedReader(
                        new InputStreamReader(connection.getInputStream(), "utf-8"));
                String line = null;  
                while ((line = br.readLine()) != null) {  
                    sb.append(line + "\n");  
                }
                br.close();
                sc.log("x: " + sb.toString());  
            	sc.log("error returned " + connection.getResponseCode());
            }
        } catch (MalformedURLException e) {
            sc.log("e: " + e.getMessage());
        } catch (IOException e) {
            sc.log("e: " + e.getMessage());
        }
		
		
		return transactionReference;
	}
}