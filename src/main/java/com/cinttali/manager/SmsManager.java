package com.cinttali.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Random;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.servlet.ServletContext;

import com.cinttali.model.SmsEmailCode;
import com.cinttali.utility.PMF;
import com.cinttali.utility.sms.HttpRequestBody;
import com.cinttali.utility.sms.SenderClass;

public class SmsManager {
	
	final String USERNAME = "buls.getme@gmail.com";
	final String USER_ID = "Cintali";
	final String PASSWORD = "cintali";
	Random rand = new Random();
	
	public SmsManager(){
		
	}

	public boolean sendSms(String userPhoneNumber, String email, ServletContext sc){
		boolean sent = false;
		String host = "http://www.smsbuzz.ng/api/send";
		String username = "?USERNAME=" + USER_ID;
		String userId = "&USERID=" + USERNAME;
		String password = "&PASSWORD=" + PASSWORD; 
		String destination = "&DESTADDR=" + userPhoneNumber;
		String sourceAdd = "&SOURCEADDR=" + USER_ID;
		//String message = "&MESSAGE=" + generateCode();
		//String params = username + userId + password + destination + sourceAdd + message;
		String smsCode = generateCode(email);
		try {
            HttpRequestBody httpBody = new HttpRequestBody();
            httpBody.setMessage(smsCode);
            httpBody.setPassword(PASSWORD);
            httpBody.setSourceAddr(USER_ID);
            httpBody.setUserId(USERNAME);
            httpBody.setUsername(USER_ID);
            httpBody.setDestAddr(userPhoneNumber);
            SenderClass senderClass = new SenderClass();
            sc.log(senderClass.send(httpBody));
        } catch (UnsupportedEncodingException ex) {
            sc.log(ex.getMessage());
        }
		
		/*try {
			host = host + URLEncoder.encode(params, "UTF-8");
		    URL url = new URL(host);
		    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		    String line;
		    String response = "";

		    while ((line = reader.readLine()) != null) {
		        response = response + line;
		        
		        sc.log("SMS GATEWAY RES: " + response);
		    }
		    reader.close();

		} catch (MalformedURLException e) {
			sc.log("SMS GATEWAY RES: " + e);
		} catch (IOException e) {
			sc.log("SMS GATEWAY RES: " + e);
		}*/
		
		return sent;
	}
	
	private String  generateCode(String email){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		String msg = "Your verification code is: ";		
	    int randomNum = rand.nextInt((99999 - 10000) + 1) + 10000;
	    SmsEmailCode userCode = new SmsEmailCode(email, randomNum);
	    pm.makePersistent(userCode);
	    pm.close();
	    
		return msg + randomNum;
	}

	public SmsEmailCode confirmSmsCode(String email, String userCode, ServletContext sc) {
		SmsEmailCode persistedCode = getSmsEmailCode(email);
		if(persistedCode != null){
			if(userCode.equals(((Integer)persistedCode.getCode()).toString())){
				persistedCode.setCode(0);
				PersistenceManager pm = PMF.get().getPersistenceManager();
				pm.makePersistent(persistedCode);
				pm.close();
			}
		}		
		
		//if null, we have already been confirmed so send back a object saying this
		if(persistedCode == null){
			persistedCode = new SmsEmailCode();
			persistedCode.setEmail(email);
			persistedCode.setCode(0);
			persistedCode.setEmailConfirmed(true);
		}
		
		if (persistedCode.getCode() == 0 && persistedCode.isEmailConfirmed()) {
			new OrderManager().processUnverifiedOrder(persistedCode, sc);	
		}
		return persistedCode;
	}
	
	public SmsEmailCode confirmEmail(String email, ServletContext sc) {
		SmsEmailCode persistedCode = getSmsEmailCode(email);
		if(persistedCode != null){
			persistedCode.setEmailConfirmed(true);
			PersistenceManager pm = PMF.get().getPersistenceManager();
			pm.makePersistent(persistedCode);
			pm.close();			
		}
		//if null, we have already been confirmed so send back a object saying this
		if(persistedCode == null){
			persistedCode = new SmsEmailCode();
			persistedCode.setEmail(email);
			persistedCode.setCode(0);
			persistedCode.setEmailConfirmed(true);
		}
		return persistedCode;
	}
	
	public SmsEmailCode appConfirmEmail(String email, ServletContext sc) {
		SmsEmailCode persistedCode = getSmsEmailCode(email);
		
		//if null, we have already been confirmed so send back a object saying this
		if(persistedCode == null){
			persistedCode = new SmsEmailCode();
			persistedCode.setEmail(email);
			persistedCode.setCode(0);
			persistedCode.setEmailConfirmed(true);
		}
		if (persistedCode.getCode() == 0 && persistedCode.isEmailConfirmed()) {
			new OrderManager().processUnverifiedOrder(persistedCode, sc);	
		}
		return persistedCode;
	}
	
	private SmsEmailCode getSmsEmailCode(String email){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		SmsEmailCode persistedCode = null;
		try{
		    persistedCode = pm.detachCopy(pm.getObjectById(SmsEmailCode.class, email));
		} catch(JDOObjectNotFoundException jonfe){
			
		}
		pm.close();
		return persistedCode;
	}
}
