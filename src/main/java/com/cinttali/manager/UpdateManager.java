package com.cinttali.manager;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.cinttali.model.DeliverySchedule;
import com.cinttali.model.MenuCategory;
import com.cinttali.model.MenuItem;
import com.cinttali.model.Outlet;
import com.cinttali.model.OutletCategory;
import com.cinttali.model.Street;
import com.cinttali.model.Update;
import com.cinttali.model.UpdateShip;
import com.cinttali.utility.PMF;
import com.cinttali.utility.PartModified;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

public class UpdateManager {
	
	public int getLatestUpdateVersion(){
		int latestVersion = 0;
		List<Update> updates = new ArrayList<Update>();
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(Update.class);	
		q.setOrdering("version desc");
		q.setRange(0,1);
		List<Update> uds = (List<Update>)q.execute(); 
		updates = uds;
		if(updates.size() > 0){
			for(Update u : updates){
				latestVersion = u.getVersion();
			}
		} else{ //first item most likely
		    latestVersion = 0;	
		}
        
               
		pm.close();		
		
		
		return latestVersion;
	}

	public int getLatestUpdateVersion(String loc) {
		int latestVersion = 0;
		List<Update> updates = new ArrayList<Update>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(Update.class);
		q.setFilter("location == l");
		q.declareParameters("String l");
		q.setOrdering("version desc");
		q.setRange(0,1);
		List<Update> uds = (List<Update>)q.execute(loc); 
		updates = uds;
		if(updates.size() > 0){
			for(Update u : updates){
				latestVersion = u.getVersion();
			}
		} else{ //first item most likely
		    latestVersion = 0;	
		}
               
		pm.close();		
		return latestVersion;
	}
	
	public UpdateShip getUpdate(String loc, String appVersion){
		UpdateShip updateShip = new UpdateShip();
		updateShip.setLatestVersion(this.getLatestUpdateVersion(loc));
		PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(Update.class);
		q.setFilter("location == l && version > v");
		q.declareParameters("String l, int v");
		q.setOrdering("version asc");				
		List<Update> uds = (List<Update>)q.execute(loc, Integer.parseInt(appVersion)); 
		for(Update u : uds){
			//if(u.getAction().equals(PartModified.UPDATE_ACTION)){
				updateShip.setUpdates(pm.detachCopy(u));
			//}
			if(u.getAction().equals(PartModified.CREATE_ACTION)){
				if(u.getType().equals(PartModified.DELIVERY_SCHEDULE_TYPE)){
					DeliverySchedule ds = new OutletManager()
					.getDeliverySchedule(u.getItemCode());
					if(ds != null){
						updateShip.setCreateDelvSchedule(ds);	
					}
					
				} else if(u.getType().equals(PartModified.MENU_CATEGORY_TYPE)){
					MenuCategory mc = new ItemManager()
					.getCategory(u.getItemCode());
					if(mc != null){
						updateShip.setCreateMenuCat(mc);	
					}
					
				} else if(u.getType().equals(PartModified.MENU_ITEM_TYPE)){
					MenuItem mi = new ItemManager()
					.getMenuItem(u.getItemCode());
					if(mi != null){
					    mi.setImageKey("");
					    updateShip.setCreateMenuItem(mi);
					}
				} else if(u.getType().equals(PartModified.OUTLET_TYPE)){
					Outlet o = new OutletManager()
					.getOutlet(u.getItemCode());
					if(o != null){
						o.setImageKey("");
						updateShip.setCreateOutlet(o);	
					}
					
				}  else if(u.getType().equals(PartModified.OUTLET_CAT_TYPE)){
					OutletCategory oc = new OutletManager()
					.getOutletCategory(u.getItemCode());
					if(oc != null){						
						updateShip.setCreateOutletCategory(oc);	
					}
					
				} else if(u.getType().equals(PartModified.STREET_TYPE)){
					Street s = new LocationManager()
					.getStreet(u.getItemCode());
					if(s != null){
						updateShip.setCreateStreet(s);	
					}
					
				}
			}
			
		}
		
		//get the ratings for outlets in this location and add to updateship
		List<Outlet> locationOutlets = new OutletManager().getOutlets(loc);
		for(Outlet o : locationOutlets){
			updateShip.setOutletRatings(o.getOutletCode(), o.getRating());
		}
		
		return updateShip;
	}
	
	public void logMenuItemUpdate(MenuItem oldItem, MenuItem newItem){
		int lv = getLatestUpdateVersion();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		if(oldItem == null){ // we have a new item created
			Update u = new Update(lv);
			u.setItemCode(newItem.getItemCode());
			u.setPartModified(PartModified.NEW_MENU_ITEM);
			u.setDetails("");
			u.setAction(PartModified.CREATE_ACTION);
			u.setLocation(newItem.getLocation());
			u.setType(PartModified.MENU_ITEM_TYPE);
			pm.makePersistent(u);
			lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
		} else{ //an update has been made
			//find and log what was changed
			//is it the name?
			if(!oldItem.getName().equals(newItem.getName())){
				Update u = new Update(lv);
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_NAME);
				u.setDetails(newItem.getName());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
			}
			//is it the price?
			if(oldItem.getPrice() != newItem.getPrice()){
				
				Update u = new Update(lv);
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_PRICE);
				u.setDetails(((Integer)newItem.getPrice()).toString());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
				
			}
			//is it the category?
			if(!oldItem.getCategory().equals(newItem.getCategory())){
				Update u = new Update(lv);
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_CATEGORY);
				u.setDetails(newItem.getCategory());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
			}
			//is it the package description?
			if(!oldItem.getPckDesc().equals(newItem.getPckDesc())){
				Update u = new Update(lv);
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_PCKDESC);
				u.setDetails(newItem.getPckDesc());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
			}
			//is it the nutrition?
			if(!oldItem.getNutrition().equals(newItem.getNutrition())){
				Update u = new Update(lv);
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_NUTRITION);
				u.setDetails(newItem.getNutrition());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
			}
			//is it the image url?
			if(!oldItem.getImageUrl().equals(newItem.getImageUrl())){
				Update u = new Update(lv);
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_IMAGE_URL);
				u.setDetails(newItem.getImageUrl());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
			}
			//is it the availability?
			if(oldItem.isAvailable() != newItem.isAvailable()){
				Update u = new Update(lv);
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_AVAILABILITY);
				u.setDetails(((Boolean)newItem.isAvailable()).toString());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
			}
		}
		pm.close();
	}
	
public void removeMenuItem(String itemCode, String location) { //here because you cannot delete before updating
		int lv = getLatestUpdateVersion();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try{
			if(itemCode != null && !itemCode.equals("")){
				MenuItem mi = pm.getObjectById(MenuItem.class, itemCode);
				BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
				blobstoreService.delete(new BlobKey(mi.getImageKey()));
				pm.deletePersistent(mi);				
				Update u = new Update(lv);
				u.setItemCode(itemCode);
				u.setPartModified(PartModified.REMOVE_MENU_ITEM);
				u.setDetails("");
				u.setAction(PartModified.REMOVE_ACTION);
				u.setLocation(location);
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
			}
		} catch(Exception e){
			System.out.println("removeMenuItem error: " + e);	
			
			
		}
		pm.close();
		
		
	}

	public void logMenuCategoryUpdate(MenuCategory oldItem, MenuCategory newItem){
		int lv = getLatestUpdateVersion();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		if(oldItem == null){ // we have a new item created
			Update u = new Update(lv);
			u.setItemCode(newItem.getCatCode());
			u.setPartModified(PartModified.NEW_MENU_CATEGORY);
			u.setDetails("");
			u.setAction(PartModified.CREATE_ACTION);
			u.setLocation(newItem.getLocation());
			u.setType(PartModified.MENU_CATEGORY_TYPE);
			pm.makePersistent(u);
			lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
		} else{ //an update has been made
	/*			//find and log what was changed
			//is it the name?
			if(!oldItem.getName().equals(newItem.getName())){
				Update u = new Update();
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_NAME);
				u.setDetails(newItem.getName());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
			}
			//is it the price?
			if(oldItem.getPrice() != newItem.getPrice()){
				Update u = new Update();
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_PRICE);
				u.setDetails(((Integer)newItem.getPrice()).toString());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
				
			}
			//is it the category?
			if(!oldItem.getCategory().equals(newItem.getCategory())){
				Update u = new Update();
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_CATEGORY);
				u.setDetails(newItem.getCategory());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
			}
			//is it the package description?
			if(!oldItem.getPckDesc().equals(newItem.getPckDesc())){
				Update u = new Update();
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_PCKDESC);
				u.setDetails(newItem.getPckDesc());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
			}
			//is it the nutrition?
			if(!oldItem.getNutrition().equals(newItem.getNutrition())){
				Update u = new Update();
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_NUTRITION);
				u.setDetails(newItem.getNutrition());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
			}
			//is it the image url?
			if(!oldItem.getImageUrl().equals(newItem.getImageUrl())){
				Update u = new Update();
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_IMAGE_URL);
				u.setDetails(newItem.getImageUrl());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
			}
			//is it the availability?
			if(oldItem.isAvailable() != newItem.isAvailable()){
				Update u = new Update();
				u.setItemCode(oldItem.getItemCode());
				u.setPartModified(PartModified.MENU_ITEM_AVAILABILITY);
				u.setDetails(((Boolean)newItem.isAvailable()).toString());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldItem.getLocation());
				u.setType(PartModified.MENU_ITEM_TYPE);
				pm.makePersistent(u);
			}*/
		}
		pm.close();
	}
	
	
	public void logDeliveryScheduleUpdate(DeliverySchedule oldDs, DeliverySchedule newDs){
		int lv = getLatestUpdateVersion();
		PersistenceManager pm = PMF.get().getPersistenceManager();		
		if(oldDs == null){ // we have a new item created
			Update u = new Update(lv);
			u.setItemCode(newDs.getDsCode());
			u.setPartModified(PartModified.NEW_DELIVERY_SCHEDULE);
			u.setDetails("");
			u.setAction(PartModified.CREATE_ACTION);
			u.setLocation(newDs.getLocation());
			u.setType(PartModified.DELIVERY_SCHEDULE_TYPE);
			pm.makePersistent(u);
			lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
		} else{ //an update has been made
			//find and log what was changed
			//is it the time?
			if(!oldDs.getTime().equals(newDs.getTime())){
				Update u = new Update(lv);
				u.setItemCode(oldDs.getDsCode());
				u.setPartModified(PartModified.DELIVERY_SCHEDULE_TIME);
				u.setDetails(newDs.getTime());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldDs.getLocation());
				u.setType(PartModified.DELIVERY_SCHEDULE_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
			}
			//is it the lopt?
			if(!oldDs.getLopt().equals(newDs.getLopt())){
				Update u = new Update(lv);
				u.setItemCode(oldDs.getDsCode());
				u.setPartModified(PartModified.DELIVERY_SCHEDULE_LOPT);
				u.setDetails(newDs.getLopt());
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldDs.getLocation());
				u.setType(PartModified.DELIVERY_SCHEDULE_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
			}
			//is it active?
			if(oldDs.isActive() != newDs.isActive()){
				Update u = new Update(lv);
				u.setItemCode(oldDs.getDsCode());
				u.setPartModified(PartModified.DELIVERY_SCHEDULE_ACTIVITY);
				u.setDetails(Boolean.toString(newDs.isActive()));
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldDs.getLocation());
				u.setType(PartModified.DELIVERY_SCHEDULE_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this Update method will use a higher update versio number
			}
			
		}
		pm.close();
	}
	
	public void logOutletUpdate(Outlet oldOutlet, Outlet newOutlet){
		int lv = getLatestUpdateVersion();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		if(oldOutlet == null){ // we have a new item created
			Update u = new Update(lv);
			u.setItemCode(newOutlet.getOutletCode());
			u.setPartModified(PartModified.NEW_OUTLET);
			u.setDetails("");
			u.setAction(PartModified.CREATE_ACTION);
			u.setLocation(newOutlet.getLocation());
			u.setType(PartModified.OUTLET_TYPE);
			pm.makePersistent(u);
			lv++; //so the next update in this Update method will use a higher update versio number
		} else{ //an update has been made
			//find and log what was changed
			//is it the activeness?
			if(oldOutlet.isActive() != newOutlet.isActive()){
				Update u = new Update(lv);
				u.setItemCode(oldOutlet.getOutletCode());
				u.setPartModified(PartModified.OUTLET_ACTIVE);
				u.setDetails(Boolean.toString(newOutlet.isActive()));
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldOutlet.getLocation());
				u.setType(PartModified.OUTLET_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this Update method will use a higher update versio number
			}
			//is it the delivery charge?
			if(oldOutlet.getDeliveryCharge() != newOutlet.getDeliveryCharge()){
				Update u = new Update(lv);
				u.setItemCode(oldOutlet.getOutletCode());
				u.setPartModified(PartModified.OUTLET_DELIVERY_CHARGE);
				u.setDetails(Integer.toString(newOutlet.getDeliveryCharge()));
				u.setAction(PartModified.UPDATE_ACTION);
				u.setLocation(oldOutlet.getLocation());
				u.setType(PartModified.OUTLET_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this Update method will use a higher update versio number
			}
			
		}
		pm.close();
	}

	public void logStreetUpdate(Street oldStreet, Street newStreet) {
		int lv = getLatestUpdateVersion();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		if(oldStreet == null){ // we have a new street created
			Update u = new Update(lv);
			u.setItemCode(newStreet.getStreetCode());
			u.setPartModified(PartModified.NEW_STREET);
			u.setDetails("");
			u.setAction(PartModified.CREATE_ACTION);
			u.setLocation(newStreet.getLocation());
			u.setType(PartModified.STREET_TYPE);
			pm.makePersistent(u);
			lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
		}
		pm.close();
		
	}

	public void removeOutlet(String outletCode, String location) { //here because you cannot delete before updating
		int lv = getLatestUpdateVersion();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try{
			if(outletCode != null && !outletCode.equals("")){
				Outlet o = pm.getObjectById(Outlet.class, outletCode);
				BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
				blobstoreService.delete(new BlobKey(o.getImageKey()));
				pm.deletePersistent(o);				
				new LocationManager().disassociateOutlet(location, outletCode); //remove this outlet from location
				Update u = new Update(lv);
				u.setItemCode(outletCode);
				u.setPartModified(PartModified.REMOVE_OUTLET);
				u.setDetails("");
				u.setAction(PartModified.REMOVE_ACTION);
				u.setLocation(location);
				u.setType(PartModified.OUTLET_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this Update method will use a higher update versio number
			}
		} catch(Exception e){
			System.out.println("removeOutlet error: " + e);	
			
			
		}
		pm.close();
		
		
	}
	
	public void removeOutletCat(String outletCatCode, String location) { //here because you cannot delete before updating
		int lv = getLatestUpdateVersion();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try{
			if(outletCatCode != null && !outletCatCode.equals("")){
				OutletCategory oCat = pm.getObjectById(OutletCategory.class, outletCatCode);				
				pm.deletePersistent(oCat);				
				Update u = new Update(lv);
				u.setItemCode(outletCatCode);
				u.setPartModified(PartModified.REMOVE_OUTLET_CAT);
				u.setDetails("");
				u.setAction(PartModified.REMOVE_ACTION);
				u.setLocation(location);
				u.setType(PartModified.OUTLET_CAT_TYPE);
				pm.makePersistent(u);
				lv++; //so the next update in this Update method will use a higher update version number
			}
		} catch(Exception e){
			System.out.println("removeOutletcat error: " + e);	
			
			
		}
		pm.close();
		
		
	}

	public void logOutletCategoryUpdate(OutletCategory oldOutletCat, 
			OutletCategory newOutletCat) {
		int lv = getLatestUpdateVersion();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		if(oldOutletCat == null){ // we have a new item created
			Update u = new Update(lv);
			u.setItemCode(newOutletCat.getCatCode());
			u.setPartModified(PartModified.NEW_OUTLET_CAT);
			u.setDetails("");
			u.setAction(PartModified.CREATE_ACTION);
			u.setLocation(newOutletCat.getLocation());
			u.setType(PartModified.OUTLET_CAT_TYPE);
			pm.makePersistent(u);
			lv++; //so the next update in this logMenuItemUpdate method will use a higher update versio number
		}
		
	}
	
	
}
