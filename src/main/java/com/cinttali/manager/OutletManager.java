package com.cinttali.manager;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.cinttali.model.DeliverySchedule;
import com.cinttali.model.Location;
import com.cinttali.model.MenuCategory;
import com.cinttali.model.MenuItem;
import com.cinttali.model.Outlet;
import com.cinttali.model.OutletCategory;
import com.cinttali.model.OutletRating;
import com.cinttali.model.Update;
import com.cinttali.utility.PMF;
import com.cinttali.utility.PartModified;

public class OutletManager {
	
	public void createOutlet(String outletCode, String outlet, String desc, String loc,
			String category, String deliveryCharge, String imageKey, String imageUrl){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Outlet o = new Outlet(outletCode, outlet, desc, loc, category, Integer.parseInt(deliveryCharge),
				imageKey, imageUrl, "0|0");
		Location location = new LocationManager().getLocation(loc);
		if(location != null){
		    location.setOutlet(o.getOutletCode());
		    pm.makePersistentAll(o, location);
		    new UpdateManager().logOutletUpdate(null, o);
		}
		pm.close();
		
	}
	
	public void updateOutletAvailability(String outletCode, String availability){ //we already know we are updating the availability of the outlet so this update will be different from others
		PersistenceManager pm = PMF.get().getPersistenceManager();
		boolean isActive = false;
		Outlet existingOutlet = getOutlet(outletCode);
		Outlet newOutlet = new Outlet(existingOutlet.getOutletCode(),
				existingOutlet.getName(), existingOutlet.getDescription(), existingOutlet.getLocation(), existingOutlet.getCategory(),
				existingOutlet.getDeliveryCharge(), existingOutlet.getImageKey(),
				existingOutlet.getImageUrl(), existingOutlet.getRating()); //confusing, but this just means we want everything from exisiting except: see following lines
		if(availability.equals("y")){
			isActive = true;
		} else if(availability.equals("n")){
			isActive = false;
		}
		newOutlet.setActive(isActive); //this makes our newOutlet truly new		
		pm.makePersistent(newOutlet);
		pm.close();
		new UpdateManager().logOutletUpdate(existingOutlet, newOutlet);
	}
	
	public void updateOutletDeliveryChrage(String outletCode, String deliveryCharge){ //we already know we are updating the availability of the outlet so this update will be different from others
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		Outlet existingOutlet = getOutlet(outletCode);
		Outlet newOutlet = new Outlet(existingOutlet.getOutletCode(),
				existingOutlet.getName(), existingOutlet.getDescription(), existingOutlet.getLocation(), existingOutlet.getCategory(),
				0, existingOutlet.getImageKey(),
				existingOutlet.getImageUrl(), existingOutlet.getRating()); //confusing, but this just means we want everything from exisiting except: see following lines		
		newOutlet.setDeliveryCharge(Integer.parseInt(deliveryCharge)); //this makes our newOutlet truly new		
		pm.makePersistent(newOutlet);
		pm.close();
		new UpdateManager().logOutletUpdate(existingOutlet, newOutlet);
	}
	
	public void removeOutlet(String outletCode, String location){
		new UpdateManager().removeOutlet(outletCode, location);
	}

	public void createOutlets(){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Outlet o = new Outlet("gn", "Trademore General", "Wide variedty of Groceries", "tm", "tm-gc", 200, "", "", "0|0");
		Location loc = new LocationManager().getLocation("tm");
		if(loc != null){
			loc.setOutlet(o.getOutletCode());
			pm.makePersistentAll(o, loc);	
		}
		
		pm.close();
	}
	
	
	public List<Outlet> getOutlets() {
		List<Outlet> outlets = new ArrayList<Outlet>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(Outlet.class);		                
        @SuppressWarnings("unchecked")
		List<Outlet> ols = (List<Outlet>)q.execute();        
        outlets = ols;        
		pm.close();		
		return outlets;
	}

	public List<Outlet> getOutlets(String loc) {
		List<Outlet> outlets = new ArrayList<Outlet>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();	
        Location location = new LocationManager().getLocation(loc);
        if(location != null){
	        for(String outletCode : location.getOutlets()){
	        	outlets.add(this.getOutlet(outletCode));
	        }
        }
        //compute rating
        /*Outlet outlet = this.getOutlet(outletCode);
    	String rate = outlet.getRating();
		String[] rateValues = rate.split("\\|");
		Integer totalRating = Integer.parseInt(rateValues[0]);
		Integer totalRaters = Integer.parseInt(rateValues[1]);
		Double rating = totalRating.doubleValue()/totalRaters.doubleValue();
		outlet.setRating(rating.toString());
    	outlets.add(outlet);*/
		/*Query q = pm.newQuery(Outlet.class);
		q.setFilter("location == l");
		q.declareParameters("String l");
        @SuppressWarnings("unchecked")
		List<Outlet> ols = (List<Outlet>)q.execute(loc); 
        for(Outlet o : ols){
        	if(o.getLocation().equals(loc)){
        		outlets.add(o);
        	}
        }*/
               
		pm.close();		
		return outlets;
	}
	
	public Outlet getOutlet(String outletCode) {
		Outlet outlet = null;		
        PersistenceManager pm = PMF.get().getPersistenceManager();	
        try{
            outlet = pm.detachCopy(pm.getObjectById(Outlet.class, outletCode));
            outlet.setImageKey("");
        } catch(Exception e){
        	
        }
		/*Query q = pm.newQuery(Outlet.class);
		q.setFilter("outletCode == oc");
		q.declareParameters("String oc");
        @SuppressWarnings("unchecked")
		List<Outlet> ols = (List<Outlet>)q.execute(outletCode); 
        for(Outlet o : ols){
        	outlet = pm.detachCopy(o);
        }*/
               
		pm.close();		
		return outlet;
	}
	
	public OutletCategory getOutletCategory(String outletCatCode) {
		OutletCategory outletCat = null;		
        PersistenceManager pm = PMF.get().getPersistenceManager();	
        try{
            outletCat = pm.detachCopy(pm.getObjectById(OutletCategory.class, outletCatCode));            
        } catch(Exception e){
        	
        }
		pm.close();		
		return outletCat;
	}
	
	public void createDeliverySchedule(String dsCode, String day, String time,
			String lopt, String location, String active, String edit, String outlet){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		DeliverySchedule ds;
		boolean isActive = true;
		if(active.equals("y")){
			isActive = true;
		} else if(active.equals("n")){
			isActive = false;
		}
		if(edit.equals("true") ){
			DeliverySchedule oldDs = this.getDeliverySchedule(dsCode);
			ds = new DeliverySchedule(dsCode, day, time, lopt, location, isActive, outlet);
			pm.makePersistent(ds);		
			pm.close();
			new UpdateManager().logDeliveryScheduleUpdate(oldDs, ds);
		} else{
			ds = new DeliverySchedule(dsCode, day, time, lopt, location, isActive, outlet);				
			pm.makePersistent(ds);		
			pm.close();
			new UpdateManager().logDeliveryScheduleUpdate(null, ds);	
		}
		
	}

    public void updateDeliverySchedule(String dsCode, String value, String part) {
    	PersistenceManager pm = PMF.get().getPersistenceManager();
    	DeliverySchedule ds = null;
		DeliverySchedule oldDs = this.getDeliverySchedule(dsCode);
		if(part.equals("time")){
			ds = new DeliverySchedule(dsCode, oldDs.getDay(), value, oldDs.getLopt(), oldDs.getLocation(), oldDs.isActive(), oldDs.getOutlet());	
		} else if(part.equals("lopt")){
			ds = new DeliverySchedule(dsCode, oldDs.getDay(), oldDs.getTime(), value, oldDs.getLocation(), oldDs.isActive(), oldDs.getOutlet());
		} else if(part.equals("active")){
			boolean active = false;
			if(value.equals("y")){
				active = true;
			} else if(value.equals("n")){
				active = false;
			}
			ds = new DeliverySchedule(dsCode, oldDs.getDay(), oldDs.getTime(), oldDs.getLopt(), oldDs.getLocation(), active, oldDs.getOutlet());
		}
		
		pm.makePersistent(ds);		
		pm.close();
		new UpdateManager().logDeliveryScheduleUpdate(oldDs, ds);
	}
    
    public DeliverySchedule getDeliverySchedule(String dsCode) {
		DeliverySchedule deliverySchedule = null;
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try{
			if(dsCode != null && !dsCode.equals("")){
				DeliverySchedule ds = pm.getObjectById(DeliverySchedule.class, dsCode);
				deliverySchedule = ds;
				System.out.println("got a ds " + ds.getDsCode());
			}
		} catch(Exception e){
			System.out.println("getdeliveryschedule error: " + e);	
			
			return deliverySchedule;
		}
		pm.close();
		
		return deliverySchedule;
	}
    
    public List<DeliverySchedule> getDeliverySchedules(String loc) {
		List<DeliverySchedule> delvSchs = new ArrayList<DeliverySchedule>();		
        PersistenceManager pm = PMF.get().getPersistenceManager();			
		Query q = pm.newQuery(DeliverySchedule.class);
		q.setFilter("location == l");
		q.declareParameters("String l");
        @SuppressWarnings("unchecked")
		List<DeliverySchedule> dSchs = (List<DeliverySchedule>)q.execute(loc); 
        for(DeliverySchedule ds : dSchs){
        	if(ds.getLocation().equals(loc)){
        		delvSchs.add(pm.detachCopy(ds));
        	}
        }
               
		pm.close();		
		return delvSchs;
	}

    public boolean hasRatedBefore(String ratingId){
    	PersistenceManager pm = PMF.get().getPersistenceManager();
    	boolean hasRatedBefore = false;
    	OutletRating or = null;
    	try{
    	or = pm.getObjectById(OutletRating.class, ratingId);
    	} catch(Exception e){
    		hasRatedBefore = false;
    	}
    	if(or == null){
    		hasRatedBefore = false;
    	} else{
    		hasRatedBefore = true;
    	}
    	
    	return hasRatedBefore;
    }
    
	public void rateOutlet(String deviceId, String outletCode, String rating) {
		if(!hasRatedBefore(deviceId+outletCode)){
			PersistenceManager pm = PMF.get().getPersistenceManager();
			OutletRating or = new OutletRating();
			or.setRatingId(deviceId, outletCode);
			or.setOutletCode(outletCode);
			or.setRating(Integer.parseInt(rating));
			Outlet ratedOutlet = this.getOutlet(outletCode);
			if(ratedOutlet != null){
				String rate = ratedOutlet.getRating();
				String[] rateValues = rate.split("\\|");
				Integer totalRating = Integer.parseInt(rateValues[0]);
				Integer totalRaters = Integer.parseInt(rateValues[1]);
				//do this rating
				totalRating = totalRating + Integer.parseInt(rating);
				totalRaters = totalRaters + 1;
				ratedOutlet.setRating(totalRating.toString()+"|"+totalRaters.toString());
				pm.makePersistentAll(ratedOutlet, or);
			}
		}
	}
	
	public int getOutletDeliveryCharge(String outletCode){
		Outlet outlet = getOutlet(outletCode);
		int deliveryCharge = 0;
		if(outlet != null){
			deliveryCharge = outlet.getDeliveryCharge();
		}
		return deliveryCharge;
	}
	
}
